FROM python:3.7-slim

LABEL maintainer="Francis Dupuis programming4finance@gmail.com"

RUN pip3 install --upgrade pip

WORKDIR /src

# Copy the project source code from the local host to the filesystem of the container at the working directory.
COPY . .

# Install specified in dev.txt.
RUN pip3 install --no-cache-dir -r requirements/prod.txt

# Run the crawler when the container launches.
ENTRYPOINT [ "python3", "src/main.py" ]