#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys


sys.path.append('./src')

import pytest
from botocore.exceptions import ClientError

from create_table_matches import create_table_match


@pytest.mark.asyncio
async def test_create_table_match(dynamodb_resource_async, dynamo_client_async):
    # Ensure we're communicating with local dynamodb instance

    async with dynamodb_resource_async as dynamo_resource:
        table_name = "dev.francis.dupuis_spac_matches"
        # Create spac stats table if it doesn't already exist
        tbl_spac_matches = dynamo_resource.Table(table_name)
        try:
            await tbl_spac_matches.table_status in ("ACTIVE")
        except ClientError:
            print(f"Table {tbl_spac_matches.name} does not exist.")

            await create_table_match(dynamo_resource, table_name)
            status = await tbl_spac_matches.table_status

            assert status in ("ACTIVE")