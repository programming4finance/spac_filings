# !/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys

import pytest
from aiohttp import web

sys.path.append('./src')
import asyncio
import uuid
from xml_to_dict import xml_to_dict
from fetch_source import fetch_source
import json

UUID = str(uuid.uuid4())
UUID2 = str(uuid.uuid4())

INPUT_INDEX_FILING = [{'cik_code': '0001412741',
                       'filing_date': '2020-02-14',
                       'doc_type': '13F-HR',
                       'fund_name': 'J. Goldman & Co LP',
                       'qtr': '2019-QTR4',
                       'path': 'https://www.sec.gov/Archives/edgar/data/1412741/0001085146-20-000794.txt',
                       'uuid': UUID
                       }]

EXPECTED = [{'nameOfIssuer': 'ACCELERON PHARMA INC', 'titleOfClass': 'COM', 'cusip': '00434H108', 'value': '5048',
             'shrsOrPrnAmt_sshPrnamt': '95200', 'shrsOrPrnAmt_sshPrnamtType': 'SH', 'putCall': 'Call',
             'investmentDiscretion': 'SOLE', 'votingAuthority_Sole': '95200', 'votingAuthority_Shared': '0',
             'votingAuthority_None': '0', 'cik_code': '0001412741', 'filing_date': '2020-02-14', 'doc_type': '13F-HR',
             'fund_name': 'J. Goldman & Co LP', 'qtr': '2019-QTR4',
             'path': 'https://www.sec.gov/Archives/edgar/data/1412741/0001085146-20-000794.txt',
             'uuid': UUID, 'reportCalendarOrQuarter': '12-31-2019',
             'accession_number': '0001085146-20-000794', 'filed_as_of_date': '20200214', 'period_of_report': '20191231',
             'conformed_company_name': 'J. Goldman & Co LP'},
            {'nameOfIssuer': 'ZIMMER BIOMET HLDGS INC', 'titleOfClass': 'COM', 'cusip': '98956P102', 'value': '11380',
             'shrsOrPrnAmt_sshPrnamt': '76032', 'shrsOrPrnAmt_sshPrnamtType': 'SH', 'investmentDiscretion': 'SOLE',
             'votingAuthority_Sole': '76032', 'votingAuthority_Shared': '0', 'votingAuthority_None': '0',
             'cik_code': '0001412741', 'filing_date': '2020-02-14', 'doc_type': '13F-HR',
             'fund_name': 'J. Goldman & Co LP', 'qtr': '2019-QTR4',
             'path': 'https://www.sec.gov/Archives/edgar/data/1412741/0001085146-20-000794.txt',
             'uuid': UUID2, 'reportCalendarOrQuarter': '12-31-2019',
             'accession_number': '0001085146-20-000794', 'filed_as_of_date': '20200214', 'period_of_report': '20191231',
             'conformed_company_name': 'J. Goldman & Co LP'}]

HTML_STR = open('tests/fixtures/filings_exist/0001085146-20-000794.txt').read()


@pytest.fixture
def cli(loop, aiohttp_client):
    app = web.Application()
    app.router.add_get('/', get_filing)

    return loop.run_until_complete(aiohttp_client(app))


async def test_get_filing(cli):
    resp = await cli.get('/')
    assert resp.status == 200
    text = await resp.text()
    assert HTML_STR in text


async def get_filing(request):
    return web.Response(text=HTML_STR)


async def test_xml_to_dict(cli):
    source = await fetch_source(url='/', session=cli,
                                sema=asyncio.BoundedSemaphore(1))

    # Convert to dict
    results = await xml_to_dict(filing_dict=INPUT_INDEX_FILING[0], source=source)

    # Ensure cusip is converted to uppercase
    results[0]['uuid'] = UUID
    results[-1]['uuid'] = UUID2

    assert results is not None

    # assert all(v == results[0][k] for k, v in EXPECTED[0].items()) and len(EXPECTED[0]) == len(results[0])
    #
    # dictA_str = json.dumps(results[0], sort_keys=True)
    # dictB_str = json.dumps(EXPECTED[0], sort_keys=True)
    #
    # assert dictA_str == dictB_str
    #
    # assert all(v == results[-1][k] for k, v in EXPECTED[-1].items()) and len(EXPECTED[-1]) == len(results[-1])
    #
    # dictA_str = json.dumps(results[-1], sort_keys=True)
    # dictB_str = json.dumps(EXPECTED[-1], sort_keys=True)
    #
    # assert dictA_str == dictB_str
