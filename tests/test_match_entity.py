# !/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys

import pytest
from aiohttp import web

sys.path.append('./src')
import asyncio
import uuid
from xml_to_dict import xml_to_dict
from fetch_source import fetch_source
from match_entity import match
from decimal import Decimal
import json
import decimal
import os
import pickle
import get_root

with open(os.path.join(str(get_root.get_project_root()), 'src', 'spac_list_final.pkl'), 'rb') as f:
    SPACS = pickle.load(f)

UUID = str(uuid.uuid4())
UUID2 = str(uuid.uuid4())

INPUT_INDEX_FILING = [{'cik_code': '0001412741',
                       'filing_date': '2020-02-14',
                       'doc_type': '13F-HR',
                       'fund_name': 'J. Goldman & Co LP',
                       'qtr': '2019-QTR4',
                       'path': 'https://www.sec.gov/Archives/edgar/data/1412741/0001085146-20-000794.txt',
                       'uuid': UUID
                       }]

EXPECTED = [
    {'cusip': 'G0080J104', 'putCall': None, 'nameOfIssuer': 'ACT II GLOBAL ACQUISITION CO',
     'titleOfClass': 'CL A SHS', 'value': Decimal('10433'), 'shrsOrPrnAmt_sshPrnamt': Decimal('1030943'),
     'shrsOrPrnAmt_sshPrnamtType': 'SH', 'investmentDiscretion': 'SOLE',
     'votingAuthority_Sole': Decimal('1030943'), 'votingAuthority_Shared': Decimal('0'),
     'votingAuthority_None': Decimal('0'), 'cik_code': '0001412741', 'filing_date': '2020-02-14',
     'doc_type': '13F-HR',
     'fund_name': 'J. Goldman & Co LP', 'qtr': '2019-QTR4',
     'path': 'https://www.sec.gov/Archives/edgar/data/1412741/0001085146-20-000794.txt',
     'uuid': UUID, 'reportCalendarOrQuarter': '12-31-2019',
     'accession_number': '0001085146-20-000794', 'filed_as_of_date': '20200214',
     'period_of_report': '20191231',
     'conformed_company_name': 'J. Goldman & Co LP', 'market_value': Decimal('10433000'),
     'root_symbol': 'ACTTU',
     'symbol': 'ACTT', 'percent_portfolio': Decimal('0.007479700209772863162996239001')},
    {'cusip': 'G0080J104', 'putCall': 'Call', 'nameOfIssuer': 'ACT II GLOBAL ACQUISITION CO',
     'titleOfClass': 'CL A SHS', 'value': Decimal('764'), 'shrsOrPrnAmt_sshPrnamt': Decimal('763900'),
     'shrsOrPrnAmt_sshPrnamtType': 'SH', 'investmentDiscretion': 'SOLE', 'votingAuthority_Sole': Decimal('763900'),
     'votingAuthority_Shared': Decimal('0'), 'votingAuthority_None': Decimal('0'), 'cik_code': '0001412741',
     'filing_date': '2020-02-14', 'doc_type': '13F-HR', 'fund_name': 'J. Goldman & Co LP (Call)', 'qtr': '2019-QTR4',
     'path': 'https://www.sec.gov/Archives/edgar/data/1412741/0001085146-20-000794.txt',
     'uuid': UUID2, 'reportCalendarOrQuarter': '12-31-2019',
     'accession_number': '0001085146-20-000794', 'filed_as_of_date': '20200214', 'period_of_report': '20191231',
     'conformed_company_name': 'J. Goldman & Co LP', 'market_value': Decimal('764000'), 'root_symbol': 'ACTTU',
     'symbol': 'ACTT', 'percent_portfolio': Decimal('0.0005477322879580626336172842515')}
]


def decimal_default(obj):
    """
    defualt function to encode decimal during deserialization to string. Subclassing no longer recommended
    """
    if isinstance(obj, decimal.Decimal):
        return float(obj)
    raise TypeError


FILING_RESP = open('tests/fixtures/filings_exist/0001085146-20-000794.txt').read()


@pytest.fixture
def cli(loop, aiohttp_client):
    app = web.Application()
    app.router.add_get('/', get_filing)

    return loop.run_until_complete(aiohttp_client(app))


async def test_get_filing(cli):
    resp = await cli.get('/')
    assert resp.status == 200
    text = await resp.text()
    assert FILING_RESP in text


async def get_filing(request):
    return web.Response(text=FILING_RESP)


async def test_match_entity(cli):
    source = await fetch_source(url='/', session=cli,
                                sema=asyncio.BoundedSemaphore(1))

    # Convert to dict
    res = await xml_to_dict(filing_dict=INPUT_INDEX_FILING[0], source=source)
    # Match records
    results = await match(dataset=res, match_set=SPACS) if isinstance(res, list) else None

    results[0]['uuid'] = UUID
    results[1]['uuid'] = UUID2

    assert results is not None
    assert len(results) == 24

    # assert all(v == results[0][k] for k, v in EXPECTED[0].items()) and len(EXPECTED[0]) == len(results[0])
    #
    # dictA_str = json.dumps(results[0], sort_keys=True, default=decimal_default)
    # dictB_str = json.dumps(EXPECTED[0], sort_keys=True, default=decimal_default)
    #
    # assert dictA_str == dictB_str
    #
    # assert all(v == results[1][k] for k, v in EXPECTED[1].items()) and len(EXPECTED[1]) == len(
    #     results[1])
    #
    # dictA_str = json.dumps(results[1], sort_keys=True, default=decimal_default)
    # dictB_str = json.dumps(EXPECTED[1], sort_keys=True, default=decimal_default)
    #
    # assert dictA_str == dictB_str
