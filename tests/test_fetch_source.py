#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import asyncio
import sys

import pytest
from aiohttp import web

sys.path.append('./src')

from fetch_source import fetch_source

HTML_STR = """
        <!DOCTYPE html>
        <html>
        <body>

        <h1>My First Heading</h1>

        <p>My first paragraph.</p>

        </body>
        </html>
        """


@pytest.fixture
def cli(loop, aiohttp_client):
    app = web.Application()
    app.router.add_get('/', hello)
    return loop.run_until_complete(aiohttp_client(app))


async def hello(request):
    return web.Response(text=HTML_STR)


async def test_hello(cli):
    resp = await cli.get('/')
    assert resp.status == 200
    text = await resp.text()
    assert HTML_STR in text


async def test_fetch_soup(cli):
    soup = await fetch_source(url='/', session=cli, sema=asyncio.BoundedSemaphore(1))
    assert isinstance(soup, str)
