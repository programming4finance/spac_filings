# !/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys

import pytest
from aiohttp import web

sys.path.append('./src')

from get_filing_links import get_filing_links


@pytest.fixture
def cli(loop, aiohttp_client):
    app = web.Application()
    app.router.add_get('/', get_master_index)

    return loop.run_until_complete(aiohttp_client(app))


async def test_get_master_index(cli):
    resp = await cli.get('/')
    assert resp.status == 200
    text = await resp.content.read()
    assert text is not None


async def get_master_index(request):
    return web.FileResponse('tests/fixtures/master.idx')


async def test_get_filing_links(cli) -> None:
    """
    Test obtaining root dir from utils
    """
    records = await get_filing_links(url="/",
                                     filing_type="13F", session=cli)
    assert len(records) == 6
    assert isinstance(records, list)

# @pytest.mark.asyncio
# async def test_invalid_url():
#     with pytest.raises(Exception) as excinfo:
#         resp = await get_filing_links(url="https://www.sec.gov/Archives/edgar/full-index/2019/QTR4/company.idx",
#                                       filing_type="13F")
