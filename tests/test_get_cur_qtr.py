# !/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys

sys.path.append('./src')

from src.filing_helpers import FilingHelpers
import datetime


def test_get_cur_qtr() -> None:
    """
    Test obtaining root dir from utils
    """
    assert "2020-QTR2" == FilingHelpers.get_cur_quarter(datetime.date(2020, 6, 10))
    assert "2020-QTR1" == FilingHelpers.get_cur_quarter(datetime.date(2020, 2, 10))
    assert "2019-QTR4" == FilingHelpers.get_cur_quarter(datetime.date(2019, 12, 31))
    assert "2019-QTR3" == FilingHelpers.get_cur_quarter(datetime.date(2019, 9, 30))
