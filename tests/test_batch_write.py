# #!/usr/bin/env python3
# # -*- coding: utf-8 -*-
# import sys
#
# import pytest
# import asyncio
#
# sys.path.append('./src')
#
# from batch_writer import batch_write
#
# TABLE_NAME = "dev.francis.test_insert"
#
#
# @pytest.mark.asyncio
# @pytest.fixture(autouse=True)
# async def setup_table(dynamodb_resource_async):
#     """ setup any state tied to the execution of the given function.
#     Invoked for every test function in the module.
#     """
#
#     print(f"SETTING UP {TABLE_NAME}")
#
#     table = await dynamodb_resource_async.create_table(
#         TableName=TABLE_NAME,
#         KeySchema=[
#             {
#                 'AttributeName': 'uuid',
#                 'KeyType': 'HASH'
#             },
#             {
#                 'AttributeName': 'path',
#                 'KeyType': 'RANGE'
#             }
#         ],
#         AttributeDefinitions=[
#             {
#                 'AttributeName': 'uuid',
#                 'AttributeType': 'S'
#             },
#             {
#                 'AttributeName': 'path',
#                 'AttributeType': 'S'
#             }
#         ],
#         ProvisionedThroughput={
#             'ReadCapacityUnits': 1,
#             'WriteCapacityUnits': 25
#         }
#     )
#
#     await table.meta.client.get_waiter('table_exists').wait(TableName=TABLE_NAME)
#
#     print(f"TABLE: {TABLE_NAME} CREATED")
#
#
# @pytest.mark.asyncio
# async def test_bulk_upload(dynamodb_resource_async):
#     recordset = [
#         {"uuid": "798a46ff-74e7-44dc-8761-a6fa00c57fca", "conm": "CBSM, L.P.", "type": "13F-NT", "cik": "0000897423",
#          "date": "2018-11-09", "path": "https://www.sec.gov/Archives/edgar/data/1704823/0000897423-18-000092.txt",
#          "qtr": "2018-QTR4", "visited": 0},
#         {"uuid": "45851281-9526-4c60-80cc-e86b9973752c", "conm": "AR ASSET MANAGEMENT INC", "type": "13F-HR",
#          "cik": "0001080166", "date": "2018-05-08",
#          "path": "https://www.sec.gov/Archives/edgar/data/1080166/0001080166-18-000002.txt", "qtr": "2018-QTR2",
#          "visited": 0},
#         {"uuid": "d9b2eada-30c7-40a1-9449-c2d6c038fe0f",
#          "conm": "PRUDENTIAL GIBRALTAR FINANCIAL LIFE INSURANCE CO., LTD.",
#          "type": "13F-NT", "cik": "0001666188", "date": "2018-05-02",
#          "path": "https://www.sec.gov/Archives/edgar/data/1666188/0001666188-18-000002.txt", "qtr": "2018-QTR2",
#          "visited": 0}
#     ]
#
#     async with dynamodb_resource_async as resource:
#         table = resource.Table(TABLE_NAME)
#
#         await batch_write(dynamodb_resource=resource, recordset=recordset, table_name=TABLE_NAME,
#                           sema=asyncio.BoundedSemaphore(3))
#
#         result = await table.scan()
#
#         assert result['Count'] == 3
