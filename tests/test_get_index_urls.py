# !/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys

sys.path.append('./src')

from src.filing_helpers import FilingHelpers
import datetime
import pytest

CUR_QUARTER = int(FilingHelpers.get_cur_quarter(datetime.date.today())[-1])
CUR_YEAR = datetime.date.today().year


# Test assumes default start year is 1993
@pytest.mark.parametrize("args, expected_length",
                         [
                             ([], (((CUR_YEAR - 1993) * 4) + CUR_QUARTER)),
                             ([2019, 1, 2020, 1], 5),
                             ([2019, 1, 2020, 4], 8),
                             ([2019, 1, 2019, 1], 1),
                             ([2020, 1, 2020, 1], 1),
                             ([2020, 4, 2020, 4], 1),
                             ([2000], (((CUR_YEAR - 2000) * 4) + CUR_QUARTER)),
                             ([2005], (((CUR_YEAR - 2005) * 4) + CUR_QUARTER)),
                             ([2010], (((CUR_YEAR - 2010) * 4) + CUR_QUARTER)),
                             ([2015], (((CUR_YEAR - 2015) * 4) + CUR_QUARTER)),
                         ])
def test_get_index_urls(args, expected_length) -> None:
    """
    Test obtaining root dir from utils
    """
    assert len(FilingHelpers.get_master_index_urls(*args)) == expected_length
