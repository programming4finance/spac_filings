import pytest
import sys
import aiohttp

sys.path.append('./src')

from failed_request import retry
from aiohttp import ClientSession


@retry(Exception, retries=2, cooldown=2, backoff=2)
async def example_request(method: str, url: str):
    async with aiohttp.ClientSession(headers={"Connection": "close"}) as session:
        resp = await session.request(method=method.upper(), url=url)

        resp.raise_for_status()


@pytest.fixture
def create_session(loop):
    session = None

    async def maker(*args, **kwargs):
        nonlocal session
        session = ClientSession(*args, **kwargs)
        return session

    yield maker
    if session is not None:
        loop.run_until_complete(session.close())


@pytest.fixture
def session(create_session, loop):
    return loop.run_until_complete(create_session())


# @pytest.mark.asyncio
# async def test_400():
#     resp = await example_request(method="GET", url="https://httpstat.us/401")
#     assert resp == aiohttp.ClientResponseError


@pytest.mark.asyncio
async def test_500():
    with pytest.raises(RuntimeError) as excinfo:
        resp = await example_request(method="GET", url="https://httpstat.us/503")
