#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
from src import get_root


def test_upload_fixtures() -> None:
    """
    Test obtaining root dir from utils
    """

    root_dir = get_root.get_project_root()

    assert isinstance(root_dir, str)
    assert root_dir == os.path.join(os.path.dirname(os.path.dirname(__file__)))
