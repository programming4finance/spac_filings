#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os

import boto3
import pytest
from aioboto3 import Session
from aiobotocore.config import AioConfig
from moto import mock_dynamodb2
from pytest_dynamodb import factories

"""
session scoped fixture, that starts DynamoDB instance at it's first use and stops at the end of the tests.
"""
dynamodb_my_proc = factories.dynamodb_proc(
    delay=True)
"""
client/resource fixture that has functional scope. After each test it drops tables in DynamoDB.
"""
dynamodb_my = factories.dynamodb('dynamodb_my_proc')

os.environ["ENDPOINT_URL"] = "http://localhost:8000"


def moto_config():
    return {
        'aws_secret_access_key': 'xxx',
        'aws_access_key_id': 'xxx',
    }


@pytest.fixture
def region():
    return 'us-east-1'


@pytest.fixture
def signature_version():
    return 'v4'


@pytest.fixture
def config(signature_version):
    return AioConfig(
        signature_version=signature_version,
        read_timeout=5,
        connect_timeout=5
    )


@pytest.fixture
def dynamodb_resource_async(request, region, config, event_loop, dynamodb_my_proc):
    session = Session(region_name=region, loop=event_loop, **moto_config())

    async def f():
        return session.resource('dynamodb', region_name=region, endpoint_url="http://localhost:8000", config=config)

    resource = event_loop.run_until_complete(f())
    yield resource

    def fin():
        event_loop.run_until_complete(resource.close())

    request.addfinalizer(fin)

@pytest.fixture
def dynamo_client_async(request, region, config, event_loop, dynamodb_my_proc):
    session = Session(region_name=region, loop=event_loop, **moto_config())

    async def f():
        return session.client('dynamodb', region_name=region, endpoint_url="http://localhost:8000", config=config)

    resource = event_loop.run_until_complete(f())
    yield resource

    def fin():
        event_loop.run_until_complete(resource.close())

    request.addfinalizer(fin)

@pytest.fixture(scope='module')
def aws_credentials():
    """Mocked AWS Credentials for moto."""
    os.environ['AWS_ACCESS_KEY_ID'] = 'testing'
    os.environ['AWS_SECRET_ACCESS_KEY'] = 'testing'
    os.environ['AWS_SECURITY_TOKEN'] = 'testing'
    os.environ['AWS_SESSION_TOKEN'] = 'testing'
    os.environ["ENDPOINT_URL"] = "http://localhost:8000"


@pytest.fixture(scope='module')
def dynamo_client(aws_credentials):
    with mock_dynamodb2():
        yield boto3.client('dynamodb', region_name='us-east-1')


@pytest.fixture(scope='module')
def dynamo_resource(aws_credentials):
    with mock_dynamodb2():
        yield boto3.resource('dynamodb', region_name='us-east-1')
