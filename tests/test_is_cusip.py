# !/usr/bin/env python3
# -*- coding: utf-8 -*-
import pytest

"""
A CUSIP is a nine-character alphanumeric code that identifies a 
North American financial security for the purposes of 
facilitating clearing and settlement of trades. The CUSIP was 
adopted as an American National Standard under Accredited Standards X9.6. 

CUSIP Criterion:
    -Length is 9 characters.
    -Characters 1, 2, 3 are digits.
    -Characters 4, 5, 6, 7, 8 are either letters or digits.
    -Characters 6, 7, 8 can also be *, @, #.
    -Character 9 is a check digit.

"""


@pytest.mark.parametrize("cusip, is_cusip",
                         [
                             ('037833100', True),
                             ('17275R102', True),
                             ('38259P508', True),
                             ('594918104', True),
                             ('68389X106', False),
                             ('68389X105', True),
                             ('42227L 102', True),
                             ('G21515104', True),
                         ])
def test_is_cusip(cusip: str, is_cusip: bool):
    """
    :param cusip: cusip number as string
    :return: boolean value if str evaluates to a correct CUSIP number
    """

    # strip whitespace
    cusip = cusip.replace(" ", "").upper()

    if len(cusip) != 9:
        raise ValueError('CUSIP must be 9 characters')

    total = 0
    for i in range(8):
        c = cusip[i]
        if c.isdigit():
            v = int(c)
        elif c.isalpha():
            p = ord(c) - ord('A') + 1
            v = p + 9
        elif c == '*':
            v = 36
        elif c == '@':
            v = 37
        elif c == '#':
            v = 38

        if i % 2 != 0:
            v *= 2

        total += int(v / 10) + v % 10
    check = (10 - (total % 10)) % 10

    valid_cusip = str(check) == cusip[-1]
    assert valid_cusip is is_cusip
