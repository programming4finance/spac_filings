#!/usr/bin/env python3
"""
Local utility to teardown dynamo table
"""
import os
import botocore.session
from dotenv import load_dotenv
load_dotenv()
if "ENDPOINT_URL" in os.environ:
    endpoint_url = os.environ["ENDPOINT_URL"]
else:
    endpoint_url = None


def teardown(table_name):
    session = botocore.session.get_session()
    dynamodb = session.create_client(
        'dynamodb',
        endpoint_url=endpoint_url
    )

    params = {
        'TableName': table_name
    }

    # Delete the table
    dynamodb.delete_table(**params)

    # Wait for the table to be deleted before exiting
    print('Waiting for', table_name, '...')
    waiter = dynamodb.get_waiter('table_not_exists')
    waiter.wait(TableName=table_name)


if __name__ == "__main__":
    teardown("dev.francis.dupuis_spac_matches")