from filing_helpers import FilingHelpers
import datetime
from get_filing_links import get_filing_links


def get_rec(company_filing_name: str):
    cur_q = FilingHelpers.get_cur_quarter(datetime.date.today()).split('-')
    master_index_url = f"https://www.sec.gov/Archives/edgar/full-index/{cur_q[0]}/{cur_q[1]}/master.idx"

    paths = get_filing_links(url=master_index_url)

    return [path for path in paths if company_filing_name in path["conm"]]
