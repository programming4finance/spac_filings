import boto3
import os

if "ENDPOINT_URL" in os.environ:
    endpoint_url = os.environ["ENDPOINT_URL"]
else:
    endpoint_url = None

table_name = os.environ["MATCH_TABLE_NAME"]

"""
Example of deleting dynamodb global secondary index with client API
"""


def delete_gsi(index_name: str):
    # Create dynamo resource
    client = boto3.client(
        'dynamodb',
        endpoint_url=endpoint_url)

    client.update_table(
        TableName=table_name,
        GlobalSecondaryIndexUpdates=[{
            'Delete': {
                'IndexName': index_name
            }
        }
        ]
    )


if __name__ == "__main__":
    delete_gsi(index_name="path-index")
