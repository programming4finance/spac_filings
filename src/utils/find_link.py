#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import asyncio
import base64
import hashlib
import hmac
import json
import os
import time
from urllib.parse import quote_plus

import aiohttp

from fetch_source import fetch_source


async def link_lookup(symbol: str, session, sema) -> [str, None]:
    """
    Uses whalewhisdom API symbol lookup to get link of stock on site along with cusip and status

    :param symbol: stock symbol as string
    :return: parsed json output (link) as string
    """
    json_args = '{"command":"stock_lookup", "symbol":"' + symbol + '"}'

    secret_key = os.environ["WHALEWISDOM_SECRET"]

    shared_key = os.environ["WHALEWISDOM_SHARED"]

    formatted_args = quote_plus(json_args)

    timenow = time.strftime('%Y-%m-%dT%H:%M:%SZ', time.gmtime())

    digest = hashlib.sha1

    raw_args = json_args + '\n' + timenow

    hmac_hash = hmac.new(secret_key.encode(), raw_args.encode(), digest).digest()

    sig = base64.b64encode(hmac_hash).rstrip()

    url_base = 'https://whalewisdom.com/shell/command.json?'

    url_args = 'args=' + formatted_args

    url_end = '&api_shared_key=' + shared_key + '&api_sig=' + sig.decode() + '&timestamp=' + timenow

    api_url = url_base + url_args + url_end

    body = await fetch_source(url=api_url, session=session, sema=sema)

    try:
        resp = json.loads(body)["stocks"]
        if len(resp) == 1:
            # We want an exact match on symbol for validation purposes.
            # We don't need every record possible
            return resp[0] if resp[0]['status'] == 'Active' and resp[0][
                'ticker'].strip().upper() == symbol.strip().upper() else None
        else:
            # Can return multiple symbols, again we want an exact match on symbol
            for d in json.loads(body)["stocks"]:
                if d["ticker"].strip().upper() == symbol.strip().upper() and d is not None:
                    return d
                else:
                    continue
    except IndexError:
        return None


async def main():
    async with aiohttp.ClientSession(headers={"Connection": "close"}) as session:
        for i in ['SBE/U', 'SBE']:
            l = await link_lookup(i, session, asyncio.BoundedSemaphore(1))
            print(l)


if __name__ == "__main__":
    #{'SBE.U': ['87105M201'], 'SBE/W': ['87105M110'], 'SBE': ['87105M102']}
                # '87105M201'
    asyncio.run(main())