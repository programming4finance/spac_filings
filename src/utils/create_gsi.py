""" This module is a Lambda Function used to create multiple
Global Secondary Indexes on a DynamoDB Table
"""

import math
import time

from log_config import logger


async def check_table_status(client, tablename: str):
    """ Check to see if a dynamo table is in an ACTIVE state'
    Dynamo tables are updated async, and an update needs to be completed
    before another update can be started otherwise an exception is raised.
    Args:
        tablename: name of the table to check status
    Returns:
        table_active: [bool] if the table is in an active state
    """
    response = await client.describe_table(
        TableName=tablename
    )
    try:
        table_status = response['Table']['TableStatus']
        if table_status == 'ACTIVE':
            table_active = True
        else:
            table_active = False
    except:
        logger.debug('Table Status not returned')
        table_active = True
    logger.debug(table_status)
    return table_active


async def check_gsi_status(client, tablename: str):
    """ Check to see that all GSIs on a dynamo table are in the ACTIVE state
    Args:
        tablename: name of the table to check status
    Returns:
        gsis_active: [bool] if the table is in an active state
    """
    response = await client.describe_table(
        TableName=tablename
    )
    try:
        if "GlobalSecondaryIndexes" in response['Table']:
            GSIs = [n['IndexStatus'] for n in response['Table']['GlobalSecondaryIndexes']]
            if all([n == 'ACTIVE' for n in GSIs]):
                gsi_active = True
            else:
                gsi_active = False
        else:
            logger.info('No GSIs present, continuing...')
            gsi_active = True
    except:
        gsi_active = True
        logger.debug('Failed to get GSIs')
    return gsi_active


async def table_active_wait(client, table_name: str, wait_seconds: int = 15):
    """Wait for table and its GSIs to be active
    Poll dynamo table status and wait for it to have a state of ACTIVE.
    The wait time between iterations is an increasing backoff.
    Args:
        table_name: name of the dynamo table for which we want a status check
        wait_seconds: number of seconds to wait in between pollings
    Returns: N/A
    """
    table_active = False
    retry = 0

    while not table_active:
        if retry > wait_seconds:
            retry = wait_seconds
        exp_wait = math.floor(wait_seconds ** (retry / wait_seconds))
        logger.debug(f"Table [{table_name}] not active, waiting [{exp_wait}] seconds to poll again")
        time.sleep(exp_wait)
        table_active = await check_table_status(client, table_name)
        logger.debug(table_active)
        retry += 1
    logger.debug(f"Table [{table_name}] is active")

    # check that the GSIs are all active
    gsi_active = False
    retry = 0
    while not gsi_active:
        if retry > wait_seconds:
            retry = wait_seconds
        exp_wait = math.floor(wait_seconds ** (retry / wait_seconds))
        logger.debug(f"Table [{table_name}] GSIs not active, waiting [{exp_wait}] seconds to poll again")
        time.sleep(exp_wait)
        gsi_active = await check_gsi_status(client, table_name)
        logger.debug(gsi_active)
        retry += 1
    logger.debug(f"Table [{table_name}] GSIs are active")
    return


async def create_gsi_1(client, table_name):
    """Create GSI 1
    Creates the Global Secondary Indexes for GSI 1
    in DynamoDB.
    Returns:
        N/A
    """
    try:
        logger.info("ADDING GSI 1")

        await client.update_table(
            AttributeDefinitions=[
                {
                    'AttributeName': 'fund_name',
                    'AttributeType': 'S'
                }
            ],
            TableName=table_name,
            GlobalSecondaryIndexUpdates=[{
                'Create': {
                    'IndexName': "fund-index1",
                    'KeySchema': [
                        {
                            'AttributeName': 'fund_name',
                            'KeyType': 'HASH'
                        },
                    ],
                    'Projection': {
                        'ProjectionType': 'INCLUDE',
                        'NonKeyAttributes': [
                            'primary'
                        ]
                    },
                    'ProvisionedThroughput': {
                        'ReadCapacityUnits': 5,
                        'WriteCapacityUnits': 1
                    }
                }
            }
            ]
        )
        logger.info(f"GSI 1 added!")
    except Exception as e:
        raise e


async def create_gsi_2(client, table_name):
    """Create GSI 2
    Creates the Global Secondary Indexes for GSI 2
    in DynamoDB with sort key.
    Returns:
        N/A
    """
    try:
        logger.info("ADDING GSI 2")
        await client.update_table(
            AttributeDefinitions=[
                {
                    'AttributeName': 'root_symbol',
                    'AttributeType': 'S'
                },
            ],
            TableName=table_name,
            GlobalSecondaryIndexUpdates=[{
                'Create': {
                    'IndexName': "root-index",
                    'KeySchema': [
                        {
                            'AttributeName': 'root_symbol',
                            'KeyType': 'HASH'
                        }
                    ],
                    'Projection': {
                        'ProjectionType': 'INCLUDE',
                        'NonKeyAttributes': [
                            'primary'
                        ]
                    },
                    'ProvisionedThroughput': {
                        'ReadCapacityUnits': 5,
                        'WriteCapacityUnits': 1
                    }
                }
            }
            ]
        )
        logger.info(f"GSI 2 added!")
    except Exception as e:
        raise e


async def create_multiple_gsi(client, table_name):
    """Lambda Function to Update Table with GSIs
    Update specified (via environment variables) table with GSIs
    Args:
        event: Lambda Event object
        context: Lambda Context object
    Returns:
        N/A
    """
    # logger.debug(f'EVENT: {event}')
    # logger.debug(f'CONTEXT: {context.__dict__}')

    # GSI 1
    await table_active_wait(client, table_name)
    logger.info(f'Creating GSI 1 on {table_name}')
    await create_gsi_1(client, table_name)

    # GSI 2
    await table_active_wait(client, table_name)
    logger.info(f'Creating GSI 2 on {table_name}')
    await create_gsi_2(client, table_name)
