#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import itertools
import os
import pickle
import boto3
import pandas as pd
from boto3.dynamodb.conditions import Key

from load_cusip import get_match_fields
from settings import ROOT_DIR
from settings import endpoint_url


def diff_dfs(symbol: str, year: int, quarter: int):
    """
    Get difference between match set and set stored in dynamo

    :symbol: spac symbol as string
    :year: year of filing as string
    :quarter: quarter period as string
    :return
    """

    # Make directory if it doesn't already exist for processed files
    if not os.path.exists(os.path.join(ROOT_DIR, 'CSV', 'Processed', str(year) + "-QTR" + str(quarter))):
        os.makedirs(os.path.join(ROOT_DIR, 'CSV', 'Processed', str(year) + "-QTR" + str(quarter)))

    dynamo_match = find_entities(symbol=symbol, qtr=str(year) + "-QTR" + str(quarter))
    dynamo_match.sort_values(by=['Fund', 'Market Value'], ascending=[True, False], inplace=True)
    dynamo_match.to_csv(
        os.path.join(ROOT_DIR, 'CSV', 'Processed', str(year) + "-QTR" + str(quarter), f'{symbol}_holdings.csv'))

    # Load scraped data as validation from whalewisdom
    whale_df = pd.read_csv(
        os.path.join(ROOT_DIR, 'CSV', 'holdings', str(year) + "-QTR" + str(quarter), f'{symbol}_holdings.csv'))
    whale_df = whale_df[whale_df.Ranking != 'Sold All']

    # Reorder cols
    whale_df_processed = whale_df[["Fund", "Shares Heldor Principal Amt", "Market Value", "% ofPortfolio"]]

    # Filter rows with no shares
    whale_df_processed = whale_df_processed.loc[whale_df_processed['Shares Heldor Principal Amt'] != 0]
    whale_df_processed.to_csv(
        os.path.join(ROOT_DIR, 'CSV', 'Processed', str(year) + "-QTR" + str(quarter), f'WW_{symbol}_holdings.csv'))

    if len(whale_df_processed) - len(dynamo_match) > 0:
        print(f"Difference in elements for {symbol} = {str(len(whale_df_processed) - len(dynamo_match))}")
        return symbol


def find_entities(symbol: str, qtr: str) -> pd.DataFrame:
    """
    Searches and retruns for entities in spac matches table via scan/query

    :symbol: spac symbol as string
    :symbol: qtr quarter period as concatenated YYYY-QTR(Q) format string
    :return: processed pandas dataframe
    """

    qtr = qtr.upper()

    # Create dynamo resource
    dynamo_resource = boto3.resource(
        'dynamodb',
        endpoint_url=endpoint_url)

    table = dynamo_resource.Table(os.environ["MATCH_TABLE_NAME"])

    # Again, we only return 1 mb of data at a time from a scan so we need to paginate by using the LastEvaluatedKey"""
    # Return indexes meeting key condition where it wasn't visited with scan (probably not the best way to do it..)
    attrs = table.query(
        IndexName='symbol-qtr',
        KeyConditionExpression=Key('symbol').eq(symbol) & Key('qtr').eq(qtr),
        ProjectionExpression="#fund_name, #shrs, #percent_portfolio, #market_value, #filing_date, #path",
        ExpressionAttributeNames={"#fund_name": "fund_name", "#shrs": "shrsOrPrnAmt_sshPrnamt",
                                  "#percent_portfolio": "percent_portfolio", "#market_value": "market_value",
                                  "#filing_date": "filing_date", "#path": "path"
                                  })
    data = attrs['Items']
    while 'LastEvaluatedKey' in attrs:
        attrs = table.query(
            IndexName='symbol-qtr',
            ExclusiveStartKey=attrs['LastEvaluatedKey'],
            KeyConditionExpression=Key('symbol').eq(symbol) & Key('qtr').eq(qtr),
            ProjectionExpression="#fund_name, #shrs, #percent_portfolio, #market_value, #filing_date",
            ExpressionAttributeNames={"#fund_name": "fund_name", "#shrs": "shrsOrPrnAmt_sshPrnamt",
                                      "#percent_portfolio": "percent_portfolio", "#market_value": "market_value",
                                      "#filing_date": "filing_date"
                                      })

        data.extend(attrs['Items'])

    df = pd.DataFrame(data)

    # Rename cols
    df.columns = ["Market Value", "% ofPortfolio", "Fund", "Shares Heldor Principal Amt", "FilingDate"]

    # Reorder cols in df
    df = df[["Fund", "Shares Heldor Principal Amt", "Market Value", "% ofPortfolio", "FilingDate"]]

    # Convert conformed name to uppercase
    df['Fund'] = df['Fund'].str.upper()

    # Convert filing date column to date
    df['FilingDate'] = pd.to_datetime(df['FilingDate'], format='%Y-%m-%d')
    df['FilingDate'] = df['FilingDate'].dt.date

    # Convert to column to string as dollar amount
    df['Market Value'] = df['Market Value'].map('${:,.0f}'.format)

    # Convert shares column to int64
    df['Shares Heldor Principal Amt'] = df['Shares Heldor Principal Amt'].astype('int64')

    # Convert % portfolio to percentage
    df['% ofPortfolio'] = (df['% ofPortfolio'].astype('float64') * 100).round(2)

    # Filter out rows with no shares
    df = df.loc[df['Shares Heldor Principal Amt'] != 0]

    # Here we group by fund name and take the latest date (could be the same date too...)
    df = df.sort_values('FilingDate').groupby('Fund').tail(1)

    return df


def loop(year: int, quarter: int):
    """
    Loop through directory of scraped whalewisdom quarterly files and match to dynamo records

    :param year: year of filings to look for
    :param quarter: quarter of filings to look for
    """
    # Loads symbols from spacdb
    with open("/Users/francis.dupuis/Documents/repos/spacs/src/spac_list.pkl", "rb") as f:
        spac_ = pickle.load(f)

    # List to store symbols that don't validate
    errsyms = []

    # Flatten symbols
    symbols = [",".join(i["symbols"].keys()).split(',') if isinstance(i["symbols"], dict) else i["symbols"] for i in
               spac_]

    # Chain and return single iterable of symbols only if not none
    res_symbols = list(
        itertools.chain(
            *[x.split(',') if x is not None and isinstance(x, str) else x for x in symbols if x is not None]))

    directory = os.path.join(ROOT_DIR, 'CSV', 'holdings', str(year) + "-QTR" + str(quarter))

    for file in os.listdir(directory):
        filename = os.fsdecode(file)
        if filename.endswith(".csv"):
            fname = filename.split('_')[0]
            if fname[-1].upper() != 'U' and fname in res_symbols:
                print("SEARCHING FOR " + fname)
                try:
                    sym = diff_dfs(symbol=fname, year=year, quarter=quarter)
                    errsyms.append(sym) if sym is not None else None
                except Exception as e:
                    print(fname + " " + str(e))
                    errsyms.append(fname)
            continue
        else:
            continue

    return errsyms


if __name__ == "__main__":
    df = find_entities(symbol="SFTW", qtr="2020-QTR4")
    print(df)