import os

import boto3
import pandas as pd
from boto3.dynamodb.conditions import Attr
from dotenv import load_dotenv

from settings import endpoint_url


def validate_holder(fund_name: str, period: str, sort_order: str) -> pd.DataFrame:
    """
    Method to validate mappings in rendered table per filer based on a given period
    """

    # Create dynamo resource
    dynamo_resource = boto3.resource(
        'dynamodb',
        endpoint_url=endpoint_url)

    table = dynamo_resource.Table(os.environ["RENDER_TABLE_NAME"])

    # Again, we only return 1 mb of data at a time from a scan so we need to paginate by using the LastEvaluatedKey"""
    # Return indexes meeting key condition where it wasn't visited with scan (probably not the best way to do it..)
    attrs = table.scan(
        FilterExpression=Attr('fund-name').eq(fund_name) & Attr('period-of-report').eq(period)
    )

    data = attrs['Items']
    while 'LastEvaluatedKey' in attrs:
        attrs = table.scan(
            ExclusiveStartKey=attrs['LastEvaluatedKey'],
            FilterExpression=Attr('fund-name').eq(fund_name) & Attr('period-of-report').eq(period)
        )

        data.extend(attrs['Items'])
    df = pd.DataFrame(data)

    df.sort_values(sort_order, inplace=True)

    total = f"{int(df['market-value'].sum()):,}"
    print(f"================TOTAL FOR FUND {fund_name} = ${total}================")

    return df


if __name__ == "__main__":
    for f in ["OXFORD ASSET MANAGEMENT LLP"]:
        validate_holder(fund_name=f, period='2020-06-30', sort_order='shares')
