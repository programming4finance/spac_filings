import asyncio

import aiohttp

import edgar_scraper
from fetch_user_agent import fetch_user_agent

USER_AGENT = fetch_user_agent()


async def main():
    """

    """
    async with aiohttp.ClientSession(headers={'User-Agent': USER_AGENT, "Connection": "close"}) as session:
        res = await edgar_scraper.bulk_crawl_and_write(documents=[
            {"path": "https://www.sec.gov/Archives/edgar/data/1080171/000108017121000002/0001080171-21-000002.txt"}],
            session=session)


if __name__ == "__main__":
    asyncio.run(main())
