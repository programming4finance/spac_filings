#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from selenium.webdriver.common.action_chains import ActionChains

import itertools
import os
import time
from typing import List

import pandas as pd
from driver import WebDriver
from find_link import link_lookup
from selenium.common.exceptions import StaleElementReferenceException, NoSuchElementException, \
    ElementClickInterceptedException, ElementNotInteractableException
from selenium.webdriver.support.ui import Select
from tqdm import tqdm
from load_cusip import load_spacs
from settings import get_project_root
import pickle
# Max attempts to retry stale elements amongst paginating
MAX_ATTEMPTS = 5

# Entities to load ands match
# SPAC_ = load_spacs()
with open("/Users/francis.dupuis/Documents/repos/spacs/src/spac_list.pkl", "rb") as f:
    SPAC_ = pickle.load(f)

def get_holdings(symbols: List[str], quarter: int = None, year: int = None):
    """
    Extracts specific symbol 13f holdings for particular quarter on whalewisdom.com to create validation sets

    :param symbols: symbols list of str
    :param quarter: quarter of year as int
    :param year: year of filing as int
    :return
    """

    # Make directory if it doesn't already exist
    if all(v is not None for v in [quarter, year]):
        dir_ = str(year) + '-' + 'QTR' + str(quarter)
    else:
        dir_ = "CURRENT_ALL"

    if not os.path.exists(os.path.join(str(get_project_root()), 'CSV', 'holdings', dir_)):
        os.makedirs(os.path.join(str(get_project_root()), 'CSV', 'holdings', dir_))

    with WebDriver(headless=False) as driver, tqdm(total=len(symbols), position=0, leave=True) as pbar:
        for symbol in symbols:
            if os.path.exists(os.path.join(str(get_project_root()), 'CSV', 'holdings', dir_, symbol + "_holdings.csv")):
                pbar.update(1)
                continue


            link = link_lookup(symbol)

            if not bool(link):
                continue

            dfs = []

            driver.get(link['link'])

            # If we retrieve current records, we don't need to do this step
            if all(v is not None for v in [quarter, year]):
                try:
                    # First filter quarter
                    select = Select(driver.find_element_by_id('quarter_one'))

                    # select by value
                    select.select_by_visible_text('Q' + str(quarter) + ' ' + str(year) + ' 13F Filings ')
                except NoSuchElementException:
                    continue

                driver.find_element_by_class_name('cmd-refresh').click()

                time.sleep(2)

            attempt = 1

            while True:
                try:
                    #  Get table and convert to HTML
                    table = driver.find_element_by_id('listings_table').get_attribute('outerHTML')
                    df = pd.read_html(table)[0]

                    dfs.append(df)

                    # Here we get the pagination count to see if we need to paginate forwards in table
                    pagination_nums = driver.find_element_by_class_name('pagination-info').text.split('to')[1].replace(
                        "rows",
                        "").split(
                        'of')

                    if pagination_nums[0] == pagination_nums[1]:
                        break
                    else:
                        next_btn = driver.find_element_by_class_name('pagination').find_element_by_class_name(
                            'page-next')

                        ActionChains(driver).move_to_element(next_btn).click(next_btn).perform()

                        # Wait for page to refresh
                        time.sleep(3)

                except IndexError:
                    break
                except (
                        ElementNotInteractableException, StaleElementReferenceException,
                        ElementClickInterceptedException):
                    print("RETRYING")
                    time.sleep(1)
                    if attempt == MAX_ATTEMPTS:
                        raise
                    attempt += 1
                    continue

            if len(dfs) > 1:
                df = pd.concat(dfs, axis=0, ignore_index=False)

            if len(df) == 1:
                pass
            else:
                df.to_csv(os.path.join(str(get_project_root()), "CSV", 'holdings', dir_,
                                       symbol.replace("/", ".") + "_holdings.csv"),
                          index=False)
            pbar.update(1)


def main():
    # Flatten symbols
    symbols = [",".join(i["symbols"].keys()).split(',') if isinstance(i["symbols"], dict) else i["symbols"] for i in
               SPAC_]

    # Chain and return single iterable of symbols only if not none
    res = list(
        itertools.chain(
            *[x.split(',') if x is not None and isinstance(x, str) else x for x in symbols if x is not None]))

    # Retrieve holdings and save to disk
    [get_holdings(symbols=res, quarter=i[1], year=i[0]) for i in [(2020, 1)]]


if __name__ == "__main__":
    main()

# # Periods to attempt to load for index periods
# # (1, 2018), (2, 2018), (3, 2018), (4, 2018)
# PERIODS = [(2020, 1)]
