#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os

from autologging import traced
from selenium import webdriver

from log_config import logger
from fake_useragent import UserAgent


@traced(logger, "__enter__", "__exit__", "__init__")
class WebDriver:
    def __init__(self, headless: bool = False):
        """

        :param headless:
        """
        logger.info("Initializing web_driver...")
        chrome_options = webdriver.ChromeOptions()

        if headless:
            chrome_options.add_argument("--log-level=3")
            chrome_options.add_argument("--headless")
            chrome_options.add_argument('--disable-gpu')
            chrome_options.add_argument('window-size=1200,1100')
            chrome_options.add_argument(f'user-agent={UserAgent().random}')

        self._driver = webdriver.Chrome(executable_path="/usr/local/bin/chromedriver",
                                        options=chrome_options)

    def __enter__(self):
        return self._driver

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._driver.quit()
