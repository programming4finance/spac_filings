#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import asyncio
import aiohttp

from functools import wraps
from log_config import logger


def retry(*exceptions, retries: int = 4, cooldown: int = 3, backoff=2, verbose: bool = True):
    """Decorate an async function to execute it a few times before giving up.
    Hopes that problem is resolved by another side shortly.

    Args:
        exceptions (Tuple[Exception]) : The exceptions expected during function execution
        retries (int): Number of retries of function execution.
        cooldown (int): Seconds to wait before retry.
        backoff (int): backoff multiplier e.g. value of 2 will double the delay each retry

        verbose (bool): Specifies if we should log about not successful attempts.
    """

    def wrap(func):
        @wraps(func)
        async def inner(*args, **kwargs):
            retries_count = 0
            mdelay = cooldown
            while True:
                try:
                    result = await func(*args, **kwargs)
                except exceptions as err:
                    logger.error(err)
                    # Immediately return if 400 range (client error)
                    try:
                        if err.status in range(400, 500):
                            return aiohttp.ClientResponseError
                    except AttributeError:
                        pass

                    retries_count += 1
                    message = "Exception during {} execution. " \
                              "{} of {} retries attempted Retrying in {} seconds...".format(
                        func, retries_count, retries, mdelay)

                    if retries_count > retries:
                        verbose and logger.exception(message)
                        raise RuntimeError("MAX RETRIES EXCEEDED", func.__qualname__, args, kwargs) from err
                    else:
                        verbose and logger.warning(message)

                    await asyncio.sleep(mdelay)
                    mdelay *= backoff
                else:
                    return result

        return inner

    return wrap
