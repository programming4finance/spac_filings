#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import datetime
import re
import xml.etree.ElementTree as ET
from typing import Dict, List

import xmltodict
import uuid
from filing_helpers import FilingHelpers
from log_config import logger
import collections

"""
3 components to filing

SEC HEADER

The 13F primary document (XML)

The 13F information table (XML)
"""


def flatten(d, parent_key='', sep='_'):
    """
    Flattens dictionary for dynamo batch write
    """
    items = []
    for k, v in d.items():
        k = k.split(":")[-1]
        new_key = parent_key + sep + k if parent_key else k
        if isinstance(v, collections.MutableMapping):
            items.extend(flatten(v, new_key, sep=sep).items())
        else:
            items.append((new_key, v))
    return dict(items)


async def xml_to_dict(filing_dict: Dict[str, str], source: str, **kwargs) -> List[
    Dict[str, str]]:
    """

    :return: Results as list of dictionaries
    """

    logger.info(f"Parsing filing {filing_dict['path']}")
    form_xml = source

    accession_number = re.findall(r"ACCESSION NUMBER:\t\t(.*?)\n", form_xml, flags=re.DOTALL)[0]
    filed_as_of_date = re.findall(r"FILED AS OF DATE:\t\t(.*?)\n", form_xml, flags=re.DOTALL)[0]
    # effectiveness_date = re.findall(r"EFFECTIVENESS DATE:\t\t(.*?)\n", form_xml, flags=re.DOTALL)[0]
    period_of_report = re.findall(r"CONFORMED PERIOD OF REPORT:\t(.*?)\n", form_xml, flags=re.DOTALL)[0]
    conformed_company_name = re.findall(r"COMPANY CONFORMED NAME:\t\t\t(.*?)\n", form_xml, flags=re.DOTALL)[0]

    xml_ = re.findall(r"<XML>(.*?)</XML>", form_xml, flags=re.DOTALL)

    try:
        root = ET.fromstring(xml_[0].strip("\n"))
    except IndexError:
        logger.error(f"Got error parsing: {filing_dict['path']}")
        return

    reportCalendarOrQuarter = root.find(
        "{http://www.sec.gov/edgar/thirteenffiler}formData/" \
        "{http://www.sec.gov/edgar/thirteenffiler}coverPage/" \
        "{http://www.sec.gov/edgar/thirteenffiler}reportCalendarOrQuarter").text

    filing_dict['qtr'] = FilingHelpers.get_cur_quarter(datetime.datetime.strptime(reportCalendarOrQuarter, '%m-%d-%Y'))
    filing_dict['reportCalendarOrQuarter'] = reportCalendarOrQuarter
    filing_dict['accession_number'] = accession_number
    filing_dict['filed_as_of_date'] = filed_as_of_date
    filing_dict['period_of_report'] = period_of_report
    filing_dict['conformed_company_name'] = conformed_company_name

    qtr = FilingHelpers.get_cur_quarter(
        date=datetime.datetime.strptime(filing_dict['period_of_report'], '%Y%m%d').date())

    year, quarter = qtr.split('-QTR')

    filing_dict['year'] = int(year)
    filing_dict['quarter'] = int(quarter)

    filing_dict['period_of_report'] = \
        datetime.datetime.strptime(filing_dict['period_of_report'], '%Y%m%d').date().\
            strftime('%Y-%m-%d')

    try:
        d = xmltodict.parse(xml_[1].strip("\n"))
        parsed = d[next(iter(d))][list(d[next(iter(d))].keys())[-1]]

        if isinstance(parsed, list):
            [p.update(filing_dict) for p in parsed]
            # Rename keys for name conformity
            parsed = [{k.split(":")[-1]: v for k, v in p.items() if isinstance(k, str)} for p in parsed]

        else:
            parsed.update(filing_dict)
            parsed = [{k.split(":")[-1]: v for k, v in parsed.items()}]

        # Add globally unique ID (could probably use a comprehension for this
        for p in parsed:
            uid = uuid.uuid4()
            p['uuid'] = str(uid)

        parsed_case = [dict((k, v.strip().upper()) if k == 'cusip' else
                            (k, v) for k, v in d.items()) for d in parsed]
        return [flatten(i) for i in parsed_case]
    except (KeyError, IndexError):
        logger.error(
            f"No 13F infotable found in {filing_dict['path']}")

        return filing_dict
