from typing import List


def batch_delete(table, items: List, hash_key: str, range_key: str) -> None:
    """
    Delete items in dynamo table with batchwriter
    """
    with table.batch_writer() as batch:
        for item in items:
            batch.delete_item(
                Key={
                    hash_key: {"S": f"{item[hash_key]}"},
                    range_key: {"S": f"{range_key}"},
                }
            )
