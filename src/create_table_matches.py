#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from log_config import logger


async def create_table_match(dynamo_resource, table_name: str):
    """
    Creates table on dynamo. Deletes if table exists
    :return:
    """

    table = await dynamo_resource.create_table(
        TableName=table_name,
        KeySchema=[
            {
                'AttributeName': 'symbol',
                'KeyType': 'HASH'
            },
            {
                'AttributeName': 'uuid',
                'KeyType': 'RANGE'
            }
        ],
        LocalSecondaryIndexes=[
            {
                'IndexName': 'symbol-qtr',
                'KeySchema': [
                    {
                        'KeyType': 'HASH',
                        'AttributeName': 'symbol'
                    },
                    {
                        'KeyType': 'RANGE',
                        'AttributeName': 'qtr'
                    }
                ],
                'Projection': {
                    'ProjectionType': 'ALL'
                }
            },
            {
                'IndexName': 'symbol-fund_name',
                'KeySchema': [
                    {
                        'KeyType': 'HASH',
                        'AttributeName': 'symbol'
                    },
                    {
                        'KeyType': 'RANGE',
                        'AttributeName': 'fund_name'
                    }
                ],
                'Projection': {
                    'ProjectionType': 'ALL'
                }
            },
            {
                'IndexName': 'symbol-doctype',
                'KeySchema': [
                    {
                        'KeyType': 'HASH',
                        'AttributeName': 'symbol'
                    },
                    {
                        'KeyType': 'RANGE',
                        'AttributeName': 'doc_type'
                    }
                ],
                'Projection': {
                    'ProjectionType': 'ALL'
                }
            }

        ],
        AttributeDefinitions=[
            {
                'AttributeName': 'symbol',
                'AttributeType': 'S'
            },
            {
                'AttributeName': 'uuid',
                'AttributeType': 'S'
            },
            {
                'AttributeName': 'qtr',
                'AttributeType': 'S'
            },
            {
                'AttributeName': 'fund_name',
                'AttributeType': 'S'
            },
            {
                'AttributeName': 'doc_type',
                'AttributeType': 'S'
            }
        ],
        ProvisionedThroughput={
            'ReadCapacityUnits': 5,
            'WriteCapacityUnits': 25
        }
    )

    await table.meta.client.get_waiter('table_exists').wait(TableName=table_name)

    logger.info(f"TABLE: {table_name} CREATED")
