#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import aioboto3
import aiohttp
import asyncio
import delete_records
import edgar_scraper
import load_cusip
import map_orgs
import os
import sys

from botocore.exceptions import ClientError
from create_table_matches import create_table_match
from create_table_matches_rendered import create_table_matches_rendered
from datetime import datetime
from fetch_user_agent import fetch_user_agent
from filing_helpers import FilingHelpers
from load_idx import get_indexes
from log_config import logger
from settings import endpoint_url

import pickle

from filing_helpers import FilingHelpers
from get_filing_links import get_filing_links

import aiohttp
import asyncio

from boto3.dynamodb.conditions import Attr
from boto3.dynamodb.conditions import Key

import boto3
import botocore

from boto3.dynamodb.conditions import Key
from botocore.exceptions import ClientError

import os

from settings import endpoint_url
from settings import get_project_root

# Generate random user agent for
USER_AGENT = fetch_user_agent()

period = {'QTR4': '12-31', 'QTR1': '03-31', 'QTR2': '06-30', 'QTR3': '09-30'}


async def main() -> bool:
    # First create master table if necessary
    async with aioboto3.resource('dynamodb',
                                 endpoint_url=endpoint_url
                                 ) as dynamo_resource:

        try:
            table_name = os.environ["RENDER_TABLE_NAME"]
            table = await dynamo_resource.Table(table_name)
            await table.table_status in "ACTIVE"
            logger.info(f"TABLE: {os.environ['RENDER_TABLE_NAME']} ALREADY EXISTS! PROCEEDING...")
        except ClientError:
            logger.info(f"TABLE: {os.environ['RENDER_TABLE_NAME']} DOES NOT EXIST, CREATING....")
            await create_table_matches_rendered(dynamo_resource=dynamo_resource,
                                                table_name=os.environ["RENDER_TABLE_NAME"])

        try:
            table_name = os.environ["MATCH_TABLE_NAME"]
            table = await dynamo_resource.Table(table_name)
            await table.table_status in "ACTIVE"
            logger.info(f"TABLE: {os.environ['MATCH_TABLE_NAME']} ALREADY EXISTS! PROCEEDING...")
        except ClientError:
            logger.info(f"TABLE: {os.environ['MATCH_TABLE_NAME']} DOES NOT EXIST, CREATING....")
            await create_table_match(dynamo_resource=dynamo_resource,
                                     table_name=os.environ["MATCH_TABLE_NAME"])

    # FIXME
    # await load_cusip.main()
    async with aiohttp.ClientSession(
            headers={'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:88.0) Gecko/20100101 Firefox/88.0',
                     'TE': 'Trailers',
                     'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                     'Connection': 'keep-alive',
                     'Upgrade-Insecure-Requests': '1'}) as session:
        records = await get_indexes(session=session)

        if records:
            await edgar_scraper.bulk_crawl_and_write(documents=records, session=session)
            return True
        else:
            return False


if __name__ == "__main__":

    with open("spac_list_final.pkl", "rb") as f:
        indexes = pickle.load(f)
        print(1)

    client = boto3.client("dynamodb",
                          endpoint_url=endpoint_url)

    # Create dynamo resource
    dynamo_resource = boto3.resource(
        'dynamodb',
        endpoint_url=endpoint_url)

    table = dynamo_resource.Table(os.environ["MATCH_TABLE_NAME"])


    def deleteItem(item_):
        try:
            client.delete_item(
                TableName=os.environ["MATCH_TABLE_NAME"],
                Key={
                    "uuid": {"S": f"{item_[0]}"},
                    "symbol": {"S": f"{item_[1]}"},
                }
            )
        except botocore.exceptions.ClientError as e:
            if e.response['Error']['Code'] in ['ConditionalCheckFailedException',
                                               'ParamVAlidationError']:
                logger.info(e.response['Error']['Message'])
            else:
                raise
        else:
            logger.info(f"delete successful for {item_[1]}")


    # with open("2021_qtr_1_indexes.pkl", "rb") as f:
    #     indexes = pickle.load(f)
    #     for idx in indexes:
    #         deleteItem(idx)

    files_to_get = asyncio.run(main())

    # Compare quarters to see which year we need to examine
    today = datetime.now().date()

    cur_quarter = FilingHelpers.get_cur_quarter(
        datetime.today().date()).split("-")[-1]
    period_of_report = str(datetime.today().year) + '-' + period[cur_quarter]

    if 1:
        logger.info("*" * 80)
        logger.info("CURRENT QUARTER IS: " + period_of_report)
        logger.info("*" * 80)

        start_yr = 2019

        while True:
            for i in period.values():
                dt_literal = str(start_yr) + "-" + i
                dt = datetime.strptime(dt_literal, "%Y-%m-%d").date()
                quarter = int((dt.month - 1) // 3 + 1)

                if dt_literal == period_of_report:
                    break

                if dt_literal == "2021-03-31":
                    # Delete current quarter indexes
                    logger.info("DELETING INDEXES FOR: " + dt_literal)
                    delete_records.main(period=dt_literal)
                if dt_literal == "2021-03-31":
                    # Re insert entire quarter indexes to rendered table
                    logger.info("MAPPING INDEXES FOR: ")
                    map_orgs.main(QTR=quarter, YEAR=dt.year)

                if list(period.values()).index(i) == 0:
                    start_yr += 1
            if dt_literal == period_of_report:
                break
