#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from log_config import logger


async def create_table_matches_rendered(dynamo_resource, table_name: str):
    """
    Creates table on dynamo. Deletes if table exists
    :return:
    """

    table = await dynamo_resource.create_table(
        TableName=table_name,
        KeySchema=[
            {
                'AttributeName': 'root-symbol',
                'KeyType': 'HASH'
            },
            {
                'AttributeName': 'uuid',
                'KeyType': 'RANGE'
            }
        ],
        LocalSecondaryIndexes=[
            {
                'IndexName': 'symbol-qtr',
                'KeySchema': [
                    {
                        'KeyType': 'HASH',
                        'AttributeName': 'root-symbol'
                    },
                    {
                        'KeyType': 'RANGE',
                        'AttributeName': 'period-of-report'
                    }
                ],
                'Projection': {
                    'ProjectionType': 'KEYS_ONLY'
                }
            }],
        AttributeDefinitions=[
            {
                'AttributeName': 'uuid',
                'AttributeType': 'S'
            },
            {
                'AttributeName': 'root-symbol',
                'AttributeType': 'S'
            },
            {
                'AttributeName': 'period-of-report',
                'AttributeType': 'S'
            }
        ],
        ProvisionedThroughput={
            'ReadCapacityUnits': 5,
            'WriteCapacityUnits': 25
        }
    )

    await table.meta.client.get_waiter('table_exists').wait(TableName=table_name)

    logger.info(f"TABLE: {table_name} CREATED")
