#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import itertools
import pickle
from collections import defaultdict
from decimal import Decimal
from decimal import InvalidOperation
from operator import itemgetter
from typing import Dict, List
import os
from log_config import logger

dir_path = os.path.dirname(os.path.realpath(__file__))
spac_path = os.path.join(dir_path, 'spac_list_final.pkl')


async def match(dataset: List[Dict[str, str]], match_set: List[Dict[str, str]]) -> List[
    Dict[str, str]]:
    """
    Matches SPAC entities to name_of_issue column in 13F file for individual fund

    :param dataset: complete 13f xml based filing transformed to list of dicts
    :type dataset: List of dictionaries containing string values
    :param match_set: spac data containing cusip values to match against
    :type match_set: List of dictionaries containing string values
    :return: List of dictionaries containing result metadata
    """
    dataset = sorted(dataset, key=lambda k: k['shrsOrPrnAmt_sshPrnamt'], reverse=True)

    magnitude = [True if ((Decimal(i['shrsOrPrnAmt_sshPrnamt']) * 10) > (Decimal(i['value']) * Decimal(.5))) or (
            i.get('putCall') is not None) else
                 False for i in dataset[:20]]
    # Switch to multiply by 1 or 1000 depending on SEC filing issues
    if all(magnitude):
        scalar = 1000
    else:
        scalar = 1

    for sub in dataset:
        sub['shrsOrPrnAmt_sshPrnamt'] = Decimal(sub['shrsOrPrnAmt_sshPrnamt'])
        sub['market_value'] = Decimal(sub['value']) * scalar

        if 'putCall' not in sub:
            sub['putCall'] = None

        # Convert STR cols to Decimal if necessary (Could automate dteection?)
        sub['value'] = Decimal(sub['value'])
        sub['votingAuthority_Sole'] = Decimal(sub['votingAuthority_Sole'])
        sub['votingAuthority_Shared'] = Decimal(sub['votingAuthority_Shared'])
        sub['votingAuthority_None'] = Decimal(sub['votingAuthority_None'])

    # Here we have to group and add dict values
    d = defaultdict(lambda: defaultdict(int))

    group_keys = ['cusip', 'putCall']

    for item in dataset:
        for key in list(item.keys()):
            if key not in group_keys:
                if isinstance(item[key], (int, float, complex, Decimal)) and not isinstance(item[key], bool):
                    d[itemgetter(*group_keys)(item)][key] += item[key]
                else:
                    d[itemgetter(*group_keys)(item)][key] = item[key]

    res = [{**dict(zip(group_keys, k)), **v} for k, v in d.items()]

    # List comprehension to create intersection of two list of dicts (SPACS_INFO  -> EDGAR DATASET)
    # filtered = [i for i in res for j in match_set if
    #             i["cusip"] in list(j["symbols_flattened"].values()) if j["symbols_flattened"]]

    filtered = [i for i in res for j in match_set if
                i["cusip"] in list(itertools.chain.from_iterable(j["symbols_flattened"].values())) if
                j["symbols_flattened"]]

    # Remove empty strings for dynamo except on decmimal values
    filtered_null = [{k: None if not v and not
    isinstance(v, Decimal) else v for k, v in d.items()} for d in filtered]

    if any(filtered_null):
        portfolio_value_total = sum(
            [i['value']
             for i in dataset])

        # Add root symbol
        for index, i in enumerate(filtered_null):
            for j in match_set:
                if i["cusip"] in list(itertools.chain.from_iterable(j["symbols_flattened"].values())):
                    filtered_null[index]["root_symbol"] = j["root-symbol"]
                    # filtered_null[index]["symbol"] = list(j["symbols_flattened"].keys())[
                    #     list(j["symbols_flattened"].values()).index(i["cusip"])]

                    index_pos = [(x, cusip.index(i["cusip"]))
                                 for x, cusip in enumerate(list(j["symbols_flattened"].values()))
                                 if i["cusip"] in cusip][0][0]

                    filtered_null[index]["symbol"] = list(j["symbols_flattened"].keys())[index_pos]

            # Append put/call to name if applicable
            if i['putCall'] is not None:
                # Lookup warrant value if call/put option exists

                with open(spac_path, 'rb') as f:
                    spac_data = pickle.load(f)

                    for j in spac_data:
                        if i["cusip"] in list(itertools.chain(*list(j['symbols_flattened'].values()))):
                            for spac_symbol in list(j['symbols'].keys()):
                                if j['symbols'][spac_symbol].get('type').lower() == "warrants" and "cusip" not in \
                                        j['symbols'][
                                            spac_symbol].get(
                                            'cusip').lower():
                                    cusip_symbol = spac_symbol
                                    cusip_warrant = j['symbols'][spac_symbol].get('cusip')
                                else:
                                    cusip_warrant = None
                        else:
                            cusip_warrant = None

                    # If no cusip match exists, delete the record entirely from what's captured
                    if cusip_warrant:
                        i['symbol'] = cusip_symbol
                        i['cusip'] = cusip_warrant
                    else:
                        del i
                        continue
            try:
                # represent decimals exactly
                i["percent_portfolio"] = i['value'] / portfolio_value_total
            except InvalidOperation:
                i["percent_portfolio"] = Decimal(0)
        logger.info(f"TOTAL OF {len(filtered_null)} MATCHES RETURNED")
    return filtered_null
