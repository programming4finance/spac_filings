#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
from get_root import get_project_root

ROOT_DIR = str(get_project_root())

if os.environ.get("MODE") is None or os.environ["MODE"] == "DEV":
    endpoint_url = os.environ["ENDPOINT_URL"]

    # Create output dir if it doesn't exist with img subdirectory
    if not os.path.exists(os.path.join(ROOT_DIR, "static")):
        os.makedirs(os.path.join(ROOT_DIR, 'static'))
else:
    endpoint_url = None
