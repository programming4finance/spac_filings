#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
from settings import ROOT_DIR
import asyncio
import concurrent.futures
import aiofiles
import aiohttp

from failed_request import retry
from log_config import logger


@retry(aiohttp.ServerDisconnectedError, aiohttp.ClientError,
       aiohttp.ServerTimeoutError,
       aiohttp.ClientConnectorError,
       concurrent.futures._base.TimeoutError)
async def fetch_source(url: str, session: aiohttp.ClientSession, sema: asyncio.BoundedSemaphore,
                       save_to_disk: bool = False):
    """
    Fetches source of url provided. For edgar it will typically be in XML or text format
    """
    async with sema:
        resp = await session.get(url)
        resp.raise_for_status()
        logger.info(f"Got response {resp.status} for URL: {url}")

        data_bytes = await resp.read()

        if save_to_disk:
            async with aiofiles.open(os.path.join(ROOT_DIR, 'static',
                                                  os.path.basename(url)), mode="wb") as out:
                await out.write(data_bytes)
                await out.flush()

        data = data_bytes.decode('utf-8')

        return data
