#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Retrieve all index urls matching 13f file types
"""

import datetime
from typing import List
from log_config import logger


class FilingHelpers:
    """
    Class of helper methods for obtaining EDGAR 13-f indexes
    """

    @staticmethod
    def get_cur_quarter(date: datetime.date) -> str:
        """

        Helper function to get quarter and year information from string

        :param date:

        :return: string representation of year and quarter

        """

        # Quarterly and yearly info for index http get string

        year = date.year

        quarter = int((date.month - 1) // 3 + 1)

        return str(year) + "-QTR" + str(quarter)

    @staticmethod
    def get_master_index_urls(start_year: int = None, start_quarter: int = 1, end_year: int = None,
                              end_quarter: int = None) -> List[str]:
        """
        Generate master index URL resources for desired quarterly indexes we want

        :param start_year: year to start generating quarterly index urls
        :param start_quarter: quarter to start generating quarterly index urls
        :param end_year: year to end generating quarterly index urls
        :param end_quarter: quarter to end generating quarterly index urls
        :return: list of url string

        """
        logger.info(f"Obtaining indexes for range: {start_year}-QTR{start_quarter} : {end_year}-QTR{end_quarter}")

        # If no start year is specified default to earliest possible date on edgar
        if start_year is None:
            start_year = 1993
        # Get current year and quarter
        current_year_quarter = FilingHelpers.get_cur_quarter(datetime.date.today()).split('-')

        # Make sure end quarter cannot exceed current quarter
        if (end_year is None) or (int(end_year) > int(current_year_quarter[0])) or (end_quarter is None):
            end_year = int(current_year_quarter[0])
            end_quarter = int(current_year_quarter[1][-1])

        history = []
        y = int(start_year)
        q = int(start_quarter)
        while True:
            # Reset quarter to 1
            if q > 4:
                q = 1
                y += 1

            history.append((y, q))

            if (y == end_year) and (q == end_quarter):
                break

            q += 1

        idx_filing = lambda x: f"https://www.sec.gov/Archives/edgar/full-index/{x[0]}/QTR{x[1]}/master.idx"

        index_urls = [idx_filing(i) for i in history]

        # Need to ensure order
        return sorted(index_urls, key=lambda x: x.split('full-index/')[-1])

    @staticmethod
    def report_iterator(iterable: List) -> str:
        """

        Generator function for parse master index file up to a specified point

        :param iterable: list of split lines to iterate through

        :return: line item as string if it is in accepted criteria

        """

        count = 0

        for item in iterable:

            if "---" in item:
                count += 1

                break

            count += 1

        for item in iterable[count:]:
            yield item