#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Example of creating dynamodb global secondary index with resource API
"""


async def create_gsi_path(dynamo_resource, index_name: str, table_name: str):
    # Create dynamo resource
    table = dynamo_resource.Table(table_name)

    attrdef = [
        {'AttributeName': 'filed_as_of_qtr',
         'AttributeType': 'S'},
        {'AttributeName': 'path',
         'AttributeType': 'S'}
    ]

    index = [{
        'Create': {
            'IndexName': index_name,
            'KeySchema': [
                {
                    'AttributeName': 'filed_as_of_qtr',
                    'KeyType': 'HASH'
                },
                {
                    'AttributeName': 'path',
                    'KeyType': 'RANGE'
                },
            ],
            'Projection': {
                'ProjectionType': 'KEYS_ONLY'
            },
            'ProvisionedThroughput': {
                'ReadCapacityUnits': 5,
                'WriteCapacityUnits': 25
            }
        }
    }]

    await table.update(AttributeDefinitions=attrdef, GlobalSecondaryIndexUpdates=index)
