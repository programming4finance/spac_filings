"""
Configurations for log file including optional console handler and file handler

Using autologging to trace function call
"""
import logging
import sys
from logging import Logger

from autologging import TRACE

FILE_HANDLER_FMT = "%(asctime)s - %(levelname)s - %(name)s - %(funcName)s - %(message)s"

CONSOLE_FMT = logging.Formatter(
    "%(asctime)s — %(name)s — %(levelname)s — %(message)s")


def create_logger() -> Logger:
    """
    Creates a logging object and returns it
    """
    logger = logging.getLogger("spac_match")

    logger.setLevel(TRACE)

    # Use console handling if needed
    logger.addHandler(get_console_handler())

    # with this pattern, it's rarely necessary to propagate the error up to parent
    logger.propagate = False

    return logger


def get_console_handler():
    """
    Returns console handler with configurations for logger
    """

    console_handler = logging.StreamHandler(sys.stdout)
    console_handler.setFormatter(CONSOLE_FMT)
    return console_handler


logger = create_logger()
