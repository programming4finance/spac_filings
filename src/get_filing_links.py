#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import concurrent.futures
import datetime
from typing import List, Dict, Union
from uuid import uuid4

import aiohttp

import filing_helpers
from failed_request import retry
from log_config import logger


@retry(aiohttp.ServerDisconnectedError, aiohttp.ClientError,
       aiohttp.ServerTimeoutError,
       aiohttp.ClientConnectorError,
       concurrent.futures._base.TimeoutError)
async def get_filing_links(url: str, filing_type: str, session: aiohttp.ClientSession, **kwargs) -> List[
    Dict[str, Union[str, int]]]:
    """

    Retrieve all filing urls for master index file url

    :param url: url to extract filings from
    :param filing_type: filing type of 13F, 13G, 13D

    :return: Set of urls as strings returned

    """

    if filing_type.upper().strip() not in ["13F", "13D", "13G"]:
        raise Exception('Filing type must be one of the following: (13F", "13D", "13G")')

    resp = await session.get(url)
    resp.raise_for_status()
    logger.info(f"Got response {resp.status} for URL: {url}")
    # Note, read(), json(), and text() methods load everything into memory. Use content to avoid this as bytes string instead of plain unicode
    data = await resp.content.read()
    lines = data.decode('utf-8').splitlines()

    num_lines = sum(1 for line in filing_helpers.FilingHelpers.report_iterator(lines))

    logger.info(f"parsing {num_lines} lines")

    # Here we are indexing the link to the filing provided
    try:
        filings = ['https://www.sec.gov/Archives/' + line for line in
                   filing_helpers.FilingHelpers.report_iterator(lines) if
                   filing_type.upper().strip() in line.split('|')[2]]
    except IndexError:
        raise Exception("Master index file must be used to extract indexes...")

    if 'edgar/' not in next(filing_helpers.FilingHelpers.report_iterator(lines)):
        raise IndexError('Not correctly indexing filing links!')

    logger.info(f"Found {len(filings)} in index file {url}")

    lines_delimited = [line.split('|') for line in list(set(filings))]

    # Generate index tuple list
    fields = ["uuid", "fund_name", "doc_type", "cik_code", "filing_date", "path", "filed_as_of_qtr"]

    records = [

        tuple(

            [str(uuid4()), line[1].strip(), line[2].strip(), line[-1].split('/')[-1].split('-')[0],

             line[3].strip(),

             'https://www.sec.gov/Archives/' + line[-1].strip(),

             filing_helpers.FilingHelpers.get_cur_quarter(datetime.datetime.strptime(line[3], '%Y-%m-%d')), 0]) for line
        in lines_delimited

    ]

    records_dict = [dict(zip(fields, record)) for record in records]

    return records_dict
