import datetime

import aiohttp

from filing_helpers import FilingHelpers
from get_filing_links import get_filing_links
from log_config import logger


async def get_indexes(session: aiohttp.ClientSession, **kwargs):
    """
    Method responsible for gathering new indexes to retrieve from EDGAR master index files.
    After initial bulk load retrieve filings since start date and diff with database records

    :param session: aiohttp client session
    :type session: class of aiohttp.ClientSession
    """
    logger.info("================SEARCHING FOR NEW INDEX FILES================")

    # Get current quarter
    cur_q = FilingHelpers.get_cur_quarter(datetime.date.today())

    logger.info(f"CURRENT QUARTER FOR TODAY IS {cur_q}")

    urls = FilingHelpers.get_master_index_urls(start_year=datetime.date.today().year, start_quarter=1, end_year=None,
                                               end_quarter=None)

    # Here we load all possible index files
    records = []
    [records.extend(await get_filing_links(url=url, filing_type="13F", session=session))
     for url in urls]

    return records
