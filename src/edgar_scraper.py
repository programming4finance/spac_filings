#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import asyncio
import os
import pickle
from asyncio.locks import BoundedSemaphore
from typing import Dict, List, Optional

import aiohttp

from batch_writer import batch_write
from fetch_source import fetch_source
from log_config import logger
from match_entity import match
from settings import get_project_root
from xml_to_dict import xml_to_dict

with open(os.path.join(str(get_project_root()), 'src', 'spac_list_final.pkl'), 'rb') as f:
    spac_ = pickle.load(f)


async def fetch_results(index_row: Dict[str, str], match_set: List[Dict[str, str]], session: aiohttp.ClientSession,
                        sema: BoundedSemaphore, **kwargs) -> Optional[List[Dict[str, str]]]:
    """
    Fetches spac matches and writes to dynamo

    :param index_row: EDGAR master index filing record
    :type index_row: Dict[str, str]
    :param match_set: SPAC data containing CUSIP values to match agaibst
    :type match_set: List[Dict[str, str]]
    :param session: aiohttp client session
    :type session: class of aiohttp.ClientSession
    :param sema: bounded semaphore to limit concurrent connections
    :type sema: class of asyncio.BoundedSemaphore
    :return: filing matches if any
    :return type: Optional[List[Dict[str, str]]]
    """
    if os.environ["MODE"] == "DEBUG":
        to_disk = True
    else:
        to_disk = False

    # Get filing source
    source = await fetch_source(
        url=index_row['path'],
        session=session, sema=sema, save_to_disk=to_disk)

    if isinstance(source, str):
        # Convert to dict
        results = await xml_to_dict(filing_dict=index_row, source=source)

        # Write match results to dynamo
        res = await match(dataset=results, match_set=match_set) if isinstance(results, list) else None
    else:
        logger.info(f"INVALID LINK {index_row['path']}")
        index_row['symbol'] = "None"
        await batch_write(recordset=[index_row], table_name=os.environ["MATCH_TABLE_NAME"],
                          sema=sema)
        return

    if res:
        logger.info(f"MATCH FOUND FOR FILING: {index_row['path']} - {index_row['conformed_company_name']}")

        await batch_write(recordset=res, table_name=os.environ["MATCH_TABLE_NAME"],
                          sema=sema)
    else:
        logger.info(f"NO MATCH FOUND FOR FILING {index_row['path']}")
        # Write to dynamo, include hash key
        index_row['symbol'] = "None"
        await batch_write(recordset=[index_row], table_name=os.environ["MATCH_TABLE_NAME"],
                          sema=sema)


async def bulk_crawl_and_write(documents: List[Dict[str, str]], session: aiohttp.ClientSession, **kwargs):
    """
    Crawl & write concurrently to `file` for multiple `urls`.

    :param documents: index documents from dynamodb
    :type documents: List[Dict[str, str]]
    :param session: aiohttp client session
    :type session: class of aiohttp.ClientSession
    :param sema: bounded semaphore to limit concurrent connections
    :type sema: class of asyncio.BoundedSemaphore

    :return:
    """

    logger.info(f"======SEARCHING FOR {len(documents)} DOCUMENTS======")
    # Create bounded semaphore to limit coroutine usage
    sema = asyncio.BoundedSemaphore(value=int(os.environ["SEMAPHORE_VALUE"]))

    tasks = []

    for r in documents:
        tasks.append(
            fetch_results(index_row=r, match_set=spac_, session=session, sema=sema, **kwargs)
        )

    await asyncio.gather(*tasks)
