#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import asyncio
import pickle
import re
from typing import List

import aioboto3
import aiohttp
import tqdm

from settings import endpoint_url
from utils import find_link

SUFFIXES = {"CORPORATION": "CORP", "LIMITED": "LTD", "COMPANY": "CO"}
EXTENSIONS = {"warrants": [".WS", "/WS", ".W", "W"], "units": [".UN", "/U", "/UN", ".U", "U"]}


async def load_spacs() -> List[str]:
    """
    execute table scan to return data frame populated by spac name and symbol for corresponding spac

    :return: str dict of spac entities and attributes
    """

    async with aioboto3.resource(
            'dynamodb',
            endpoint_url=endpoint_url
    ) as dynamo_resource:
        table = await dynamo_resource.Table('company')

        # Scan table with desired attributes from projection expression
        response = await table.scan(
            ProjectionExpression="#name,#symbols,#root",
            ExpressionAttributeNames={'#name': 'name', '#symbols': 'symbols', "#root": "root-symbol"}
        )

        table_data = response['Items']

        """
        To get all items from DynamoDB table, you can use Scan operation. The problem is that Scan has 1 MB limit on
        the amount of data it will return in a request, so we need to paginate through the results in a loop.
        """
        while 'LastEvaluatedKey' in response:
            response = await table.scan(ExclusiveStartKey=response['LastEvaluatedKey'],
                                        ProjectionExpression="#name,#symbols,#root",
                                        ExpressionAttributeNames={'#name': 'name', '#symbols': 'symbols',
                                                                  "#root": "root-symbol"})
            table_data.extend(response['Items'])

        table_data = sorted(table_data, key=lambda k: k['root-symbol'])

    return table_data


async def lookup(sym, session, sema):
    try:
        cusip_whale = await find_link.link_lookup(sym, session, sema)
        cusip_whale = cusip_whale['cusip']
        if bool(cusip_whale):
            print(sym + " INVALID CUSIP")
            cusip_val = re.sub(r'\W+', '', cusip_whale.strip()).upper()
            return cusip_val
    except TypeError:
        pass


async def get_match_fields(doc: str, session, sema) -> List[str]:
    """
    Return original dynamo collection with match fields added to dictionaries

    :return: List of dictionaries comprised of spac entity information
    """
    d = {}

    # This will end up being in a format of several nested dicts per root symbol comprised of multiple symbols
    if isinstance(doc["symbols"], dict):
        for p_id, p_info in doc["symbols"].items():
            symbol_lookup = p_id
            print("SEACHING FOR " + p_id)
            cusip_ = p_info.get("cusip")

            root_sym = doc['root-symbol']

            if "." in root_sym:
                # root_sym = root_sym.split(".")[0]
                root_sym = root_sym.rsplit('.', 1)[0]
            else:
                # root_sym = root_sym.split("U")[0]
                root_sym = root_sym.rsplit('U', 1)[0]

            # Values with CUSIP will be presumed delisted companies
            if cusip_:
                if re.sub(r'\W+', '', cusip_.strip()).upper() == "CUSIP":
                    # cusip_val = await find_link.link_lookup(p_id, session, sema)
                    # if cusip_val:
                    #     d[p_id] = [cusip_val]
                    continue

                cusip_val = re.sub(r'\W+', '', cusip_.strip()).upper()

                # Verify CUSIP with whalewisdom API.
                # For some symbols the warrants and common stock securities have the same CUSIP
                count = 0
                while True:
                    try:
                        cusip_whale = await find_link.link_lookup(symbol_lookup, session, sema)
                        cusip_whale = cusip_whale['cusip']
                        if cusip_whale != cusip_val and bool(cusip_whale):
                            # if p_id in ISSUES:
                            d[p_id] = [cusip_val, re.sub(r'\W+', '', cusip_whale.strip()).upper()]
                            break
                        else:
                            print(f"CUSIP VALUES MATCH: {symbol_lookup}")
                            d[p_id] = [cusip_val]
                            break
                    except TypeError:
                        print(f"No info for {symbol_lookup} on Whalewisdom...")
                        if p_info['type'] in ["warrants", "units"]:
                            try:
                                if root_sym not in p_id:
                                    # p_id = root_sym + EXTENSIONS[p_info['type']][count]
                                    d[p_id] = [cusip_val]
                                    break
                                else:
                                    symbol_lookup = root_sym + EXTENSIONS[p_info['type']][count]
                                count += 1
                            except IndexError:
                                d[p_id] = [cusip_val]
                                break
                        else:
                            d[p_id] = [cusip_val]
                            break

    return d if d else None


async def update(i, r, session, sema):
    syms = await get_match_fields(r, session, sema)
    company_collection[i]["symbols_flattened"] = syms


async def main():
    # First obtain list from spac site
    global company_collection
    company_collection = await load_spacs()
    sema = asyncio.BoundedSemaphore(2)

    async with aiohttp.ClientSession(connector=aiohttp.TCPConnector(verify_ssl=False)) as session:
        # Convert to uppercase to match Edgar convention in 13F filings for "NAME OF ISSUER" field.
        # We also remove any non alphanumeric chars here too
        for i in company_collection:
            try:
                s = re.sub('[^0-9a-zA-Z]+', ' ', i["name"].upper()).strip()
            except KeyError:
                print("NO METADATA FOR SYMBOL: " + str(i))
                continue

            clean_name = re.sub(r'\b\w+\b', lambda m: SUFFIXES.get(m.group(), m.group()), s)
            i["clean_name"] = clean_name

        tasks = []
        for i, j in enumerate(company_collection):
            if j.get("symbols"):
                tasks.append(update(i, j, session, sema))

        pbar = tqdm.tqdm(total=len(tasks))
        for f in asyncio.as_completed(tasks):
            value = await f
            pbar.set_description(value)
            pbar.update()

        spacs_list = []

        for i in company_collection:
            try:
                if bool(i['symbols_flattened']):
                    spacs_list.append(i)
            except KeyError:
                pass

        with open("spac_list_final.pkl", "wb") as f:
            pickle.dump(spacs_list, f)


if __name__ == "__main__":
    asyncio.run(main())
