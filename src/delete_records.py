from log_config import logger
import os
import pickle
from typing import Dict, List

import boto3
import botocore
from boto3.dynamodb.conditions import Key
from botocore.exceptions import ClientError

from settings import endpoint_url
from settings import get_project_root

client = boto3.client("dynamodb",
                      endpoint_url=endpoint_url)

# Create dynamo resource
dynamo_resource = boto3.resource(
    'dynamodb',
    endpoint_url=endpoint_url)

table = dynamo_resource.Table(os.environ["RENDER_TABLE_NAME"])


def find_entities(symbol: str, period: str) -> List[Dict[str, str]]:
    """
    Searches and retruns for entities in spac matches table via scan/query

    :symbol: spac symbol as string
    :symbol: qtr quarter period as concatenated YYYY-QTR(Q) format string
    :return: processed pandas dataframe
    """
    # Again, we only return 1 mb of data at a time from a scan so we need to paginate by using the LastEvaluatedKey"""
    # Return indexes meeting key condition where it wasn't visited with scan (probably not the best way to do it..)
    attrs = table.query(
        IndexName='symbol-qtr',
        KeyConditionExpression=Key('root-symbol').eq(symbol) & Key('period-of-report').eq(period),
        ProjectionExpression="#uuid, #symbol",
        ExpressionAttributeNames={"#uuid": "uuid", "#symbol": "root-symbol"})

    data = attrs['Items']
    while 'LastEvaluatedKey' in attrs:
        attrs = table.query(
            IndexName='symbol-qtr',
            ExclusiveStartKey=attrs['LastEvaluatedKey'],
            KeyConditionExpression=Key('root-symbol').eq(symbol) & Key('period-of-report').eq(period),
            ProjectionExpression="#uuid, #symbol",
            ExpressionAttributeNames={"#uuid": "uuid", "#symbol": "root-symbol"})

        data.extend(attrs['Items'])

    return data


def deleteItem(item_):
    try:
        client.delete_item(
            TableName=os.environ["RENDER_TABLE_NAME"],
            Key={
                "uuid": {"S": f"{item_['uuid']}"},
                "root-symbol": {"S": f"{item_['root-symbol']}"},
            }
        )
    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Code'] in ['ConditionalCheckFailedException',
                                           'ParamVAlidationError']:
            logger.info(e.response['Error']['Message'])
        else:
            raise
    else:
        logger.info(f"delete successful for {item_['root-symbol']}")


def main(period: str):
    logger.info("====================================")
    logger.info("       Getting ids to delete        ")
    logger.info("====================================")

    with open(os.path.join(str(get_project_root()), 'src', 'spac_list_final.pkl'), 'rb') as f:
        SPAC_ = pickle.load(f)

    for i in SPAC_:
        try:
            res = find_entities(symbol=i['root-symbol'], period=period)

            if res:
                for i in res:
                    deleteItem(item_=i)
        except (AttributeError, IndexError, KeyError) as e:
            print(str(e))
