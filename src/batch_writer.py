#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import asyncio
import concurrent.futures
from typing import Dict
from typing import List

import aioboto3
import aiohttp
from botocore.exceptions import ClientError

from failed_request import retry
from log_config import logger
from settings import endpoint_url

RETRY_EXCEPTIONS = ('ProvisionedThroughputExceededException',
                    'ThrottlingException')


@retry(aiohttp.ServerDisconnectedError, aiohttp.ClientError,
       aiohttp.ServerTimeoutError,
       aiohttp.ClientConnectorError,
       concurrent.futures._base.TimeoutError)
async def batch_write(recordset: List[Dict[str, str]], table_name: str,
                      sema: asyncio.BoundedSemaphore, **kwargs):
    """
    Batch writes recordset to dynamo collection

    :param dynamodb_resource: aiobto3 resource for dynamo
    :param recordset: List of dictionary items (indexes) to be stored in dynamodb
    :param table_name: name of dynamo collection as string
    :param sema: bounded semaphore

    :return:
    """
    # Create dynamo client
    async with aioboto3.resource('dynamodb',
                                 endpoint_url=endpoint_url
                                 ) as dynamodb_resource:
        async with sema:
            table = await dynamodb_resource.Table(table_name)

            # Batch write dict to table
            retries = 0

            while True:
                logger.info(
                    f"ATTEMPTING TO WRITE {len(recordset)} RECORD(S) TO {table_name} for symbol(s) {list(set([i['symbol'] for i in recordset if i['symbol'] != 'None']))}")
                for r in recordset:
                    try:
                        await table.put_item(Item=r)
                        retries = 0  # if successful, reset count

                    except ClientError as err:
                        if err.response['Error']['Code'] not in RETRY_EXCEPTIONS:
                            raise

                        logger.info('Exceeded throughput while writing item. Retries={}'.format(retries))
                        await asyncio.sleep(2 ** retries)
                        retries += 1  # TODO max limit
                break
