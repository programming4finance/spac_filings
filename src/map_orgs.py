#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import json
import os
import pickle
import time

import aioboto3
import boto3
import pandas as pd
from boto3.dynamodb.conditions import Key
from botocore.exceptions import ClientError

from create_table_matches_rendered import create_table_matches_rendered
from log_config import logger
from settings import endpoint_url
from settings import get_project_root

RETRY_EXCEPTIONS = ('ProvisionedThroughputExceededException',
                    'ThrottlingException')

with open(os.path.join(str(get_project_root()), 'src', 'spac_list_final.pkl'), 'rb') as f:
    SPAC_ = pickle.load(f)


def find_entities(symbol: str, qtr: str, type_dict: dict) -> pd.DataFrame:
    """
    Searches and retruns for entities in spac matches table via scan/query

    :symbol: spac symbol as string
    :symbol: qtr quarter period as concatenated YYYY-QTR(Q) format string
    :return: processed pandas dataframe
    """
    qtr = qtr.upper()

    # Create dynamo resource
    dynamo_resource = boto3.resource(
        'dynamodb',
        endpoint_url=endpoint_url)

    table = dynamo_resource.Table(os.environ["MATCH_TABLE_NAME"])

    # Again, we only return 1 mb of data at a time from a scan so we need to paginate by using the LastEvaluatedKey"""
    # Return indexes meeting key condition where it wasn't visited with scan (probably not the best way to do it..)
    attrs = table.query(
        IndexName='symbol-qtr',
        KeyConditionExpression=Key('symbol').eq(symbol) & Key('qtr').eq(qtr),
        ProjectionExpression="#uuid, #titleOfClass, #root_symbol, #year, #quarter, #fund_name, #shrs, "
                             "#market_value, #filing_date, #period_of_report, #doc_type",
        ExpressionAttributeNames={"#titleOfClass": "titleOfClass", "#root_symbol": "root_symbol",
                                  "#uuid": "uuid", "#year": "year", "#quarter": "quarter", "#fund_name": "fund_name",
                                  "#shrs": "shrsOrPrnAmt_sshPrnamt", "#market_value": "market_value",
                                  "#filing_date": "filing_date", "#period_of_report": "period_of_report",
                                  "#doc_type": "doc_type"
                                  })

    data = attrs['Items']
    while 'LastEvaluatedKey' in attrs:
        attrs = table.query(
            IndexName='symbol-qtr',
            ExclusiveStartKey=attrs['LastEvaluatedKey'],
            KeyConditionExpression=Key('symbol').eq(symbol) & Key('qtr').eq(qtr),
            ProjectionExpression="#uuid, #titleOfClass, #root_symbol, #year, #quarter, #fund_name, #shrs, "
                                 "#market_value, #filing_date, #period_of_report, #doc_type",
            ExpressionAttributeNames={"#titleOfClass": "titleOfClass", "#root_symbol": "root_symbol",
                                      "#uuid": "uuid", "#year": "year", "#quarter": "quarter",
                                      "#fund_name": "fund_name",
                                      "#shrs": "shrsOrPrnAmt_sshPrnamt", "#market_value": "market_value",
                                      "#filing_date": "filing_date", "#period_of_report": "period_of_report",
                                      "#doc_type": "doc_type"
                                      })

        data.extend(attrs['Items'])

    df = pd.DataFrame(data)

    # Rename cols
    if len(df) > 0:
        df = df.rename(
            columns={"market_value": "market-value", "shrsOrPrnAmt_sshPrnamt": "shares", "fund_name": "fund-name",
                     "filing_date": "filing-date",
                     "root_symbol": "root-symbol", "doc_type": "doc-type", "period_of_report": "period-of-report",
                     "titleOfClass": "security"})

        # Reorder cols in df
        df = df[["uuid", "root-symbol", "fund-name", "security", "period-of-report", "filing-date",
                 "shares",
                 "market-value", "year", "quarter", "doc-type"]]

        # Convert conformed name to uppercase
        df['fund-name'] = df['fund-name'].str.upper()

        # -------Data type conversions--------#
        # Convert shares column to int64
        df['shares'] = df['shares'].astype('int64')
        df['market-value'] = df['market-value'].astype('int64')

        df['year'] = df['year'].astype('int64')
        df['quarter'] = df['quarter'].astype('int64')
        # -------Data type conversions--------#

        df['security'] = type_dict['symbols'][symbol]['type'].upper()

        # --------Pre processing--------------#
        # Filter out rows with no shares
        df = df.loc[df['shares'] != 0]
        df['doc-length'] = df['doc-type'].str.len()
        df.drop('doc-type', 1, inplace=True)

        # Here we group by fund name and take the latest date (could be the same date too...)
        # df = df.sort_values('filing-date').groupby('fund-name').tail(1)
        df = df.sort_values(['fund-name', 'filing-date', 'doc-length'], ascending=[True, True, True]).groupby(
            'fund-name').tail(1)

        df.drop('filing-date', 1, inplace=True)
        df.drop('doc-length', 1, inplace=True)

        # Map to dynamo
        records = json.loads(df.T.to_json()).values()

        batch_w(list(records))
    else:
        logger.warning("No data returned for symbol --------- %s --------- %s", symbol, type_dict)


def batch_w(recordset):
    dynamodb_resource = boto3.resource('dynamodb',
                                       endpoint_url=endpoint_url
                                       )
    table = dynamodb_resource.Table(os.environ['RENDER_TABLE_NAME'])

    retries = 0
    while True:
        logger.info(f"ATTEMPTING TO WRITE {len(recordset)} RECORDS TO {os.environ['RENDER_TABLE_NAME']}")
        for r in recordset:
            try:
                table.put_item(Item=r)
                logger.info(f"WROTE symbol {r['root-symbol']}-{r['security']} to db")
                retries = 0  # if successful, reset count

            except ClientError as err:
                if err.response['Error']['Code'] not in RETRY_EXCEPTIONS:
                    raise

                logger.info('Exceeded throughput while writing item. Retries={}'.format(retries))
                time.sleep(2)
                retries += 1  # TODO max limit
        break


def main(QTR, YEAR):
    errs = []
    # Flatten symbols
    symbols = []
    for i in SPAC_:
        for j in i['symbols_flattened'].keys():
            try:
                find_entities(symbol=j, qtr=str(YEAR) + "-QTR" + str(QTR), type_dict=i)
            except (AttributeError, IndexError, KeyError, ValueError) as e:
                print("ERR " + str(e) + ' ' + str(j))
                errs.append(j)
