from boto3.dynamodb.conditions import Attr

from log_config import logger
import boto3
from batch_delete import batch_delete
from mypy_boto3_dynamodb import ServiceResource
from mypy_boto3_dynamodb import DynamoDBClient

def delete_none_type(dynamo_table, period_of_report: str) -> None:
    """
    Deletes records without a fund name for final prod rendered table

    :param period_of_report: Period of report as string. Should match YYYY-MM-DD format for current quarter minus one date literal.
        For example, 2020-12-31 for 2021 QTR-1 filings
    :type period_of_report: str
    """
    logger.info("====================================")
    logger.info("       Getting ids to delete        ")
    logger.info("====================================")

    # FIXME: create local secondary index on table for period
    attrs = dynamo_table.scan(
        FilterExpression=Attr('period-of-report').eq(period_of_report)
    )

    data = attrs['Items']
    while 'LastEvaluatedKey' in attrs:
        attrs = dynamo_table.scan(
            ExclusiveStartKey=attrs['LastEvaluatedKey'],
            FilterExpression=Attr('period-of-report').eq(period_of_report)
        )

        data.extend(attrs['Items'])

    items_to_delete = [(i["uuid"], i["root-symbol"]) for i in data if i["fund-name"] is None]

    if items_to_delete:
        batch_delete(table=dynamo_table, items=items_to_delete, hash_key="root-symbol", range_key="uuid")
    else:
        logger.info("No items to delete with no fund_name")


if __name__ == "__main__":
    breakpoint()
