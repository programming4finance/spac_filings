from log_config import logger
import os
import pickle
from typing import Dict, List
from boto3.dynamodb.conditions import Attr

import boto3
import botocore
from boto3.dynamodb.conditions import Key
from botocore.exceptions import ClientError

from settings import endpoint_url
from settings import get_project_root

client = boto3.client("dynamodb",
                      endpoint_url=endpoint_url)

# Create dynamo resource
dynamo_resource = boto3.resource(
    'dynamodb',
    endpoint_url=endpoint_url)

table = dynamo_resource.Table(os.environ["RENDER_TABLE_NAME"])


def find_entities(symbol, path) -> List[Dict[str, str]]:
    """
    Searches and retruns for entities in spac matches table via scan/query

    :symbol: spac symbol as string
    :symbol: qtr quarter period as concatenated YYYY-QTR(Q) format string
    :return: processed pandas dataframe
    """
    # Again, we only return 1 mb of data at a time from a scan so we need to paginate by using the LastEvaluatedKey"""
    # Return indexes meeting key condition where it wasn't visited with scan (probably not the best way to do it..)
    attrs = table.query(
        IndexName='path-index',
        KeyConditionExpression=Key('filed_as_of_qtr').eq(symbol) & Key('path').eq(path),
        ProjectionExpression="root_symbol",
        ExpressionAttributeNames={"root_symbol": "root_symbol"})

    data = attrs['Items']
    while 'LastEvaluatedKey' in attrs:
        attrs = table.query(
            IndexName='path-index',
            ExclusiveStartKey=attrs['LastEvaluatedKey'],
            KeyConditionExpression=Key('filed_as_of_qtr').eq(symbol) & Key('path').eq(path),
            ProjectionExpression="#uuid, #root_symbol",
            ExpressionAttributeNames={"#uuid": "uuid", "#root_symbol": "root-symbol"})

        data.extend(attrs['Items'])

    return data


def deleteItem(item_):
    try:
        client.delete_item(
            TableName=os.environ["RENDER_TABLE_NAME"],
            Key={
                "uuid": {"S": f"{item_[0]}"},
                "root-symbol": {"S": f"{item_[1]}"},
            }
        )
    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Code'] in ['ConditionalCheckFailedException',
                                           'ParamVAlidationError']:
            logger.info(e.response['Error']['Message'])
        else:
            raise
    else:
        logger.info(f"delete successful for {item_[1]}")


def main():
    logger.info("====================================")
    logger.info("       Getting ids to delete        ")
    logger.info("====================================")

    attrs = table.scan(
        FilterExpression=Attr('period-of-report').eq("2020-12-31")
    )

    data = attrs['Items']
    while 'LastEvaluatedKey' in attrs:
        attrs = table.scan(
            ExclusiveStartKey=attrs['LastEvaluatedKey'],
            FilterExpression=Attr('period-of-report').eq("2020-12-31")
        )

        data.extend(attrs['Items'])

    items_to_delete = [(i["uuid"], i["root-symbol"]) for i in data if i["fund-name"] is None]
    for item in items_to_delete:
        deleteItem(item)

        # pickle.dump([(i["uuid"], i["root-symbol"]) for i in data if "fund-name" not in i.keys()], f)
        #
        # with open("2021_qtr_1_indexes.pkl", "rb") as f:
        #     indexes = pickle.load(f)
        #     for idx in indexes:
        #         deleteItem(idx)
        #
        # print(res)


main()
